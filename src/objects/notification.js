export function Notification (title = '', text = [''], icon = '', color = '', spin = false, image = '') {
  this.title = title
  this.text = text
  this.icon = icon
  this.color = color
  this.spin = spin
  this.image = image
}