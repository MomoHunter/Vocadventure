import { defineStore } from 'pinia'
import { useSavestateStore } from './savestate'
import { useAppConstStore } from './appconst'

export const useGameDynStore = defineStore({
  id: 'gamedyn',
  state: () => ({
    vocabs: null,
    answers: [],
    startTime: 0, // start time of the adventure
    webglCanvas: null,
    webglContext: null,
    basicCanvas: null,
    basicContext: null,
    loopActivated: false,
    raf: null,
    lag: 0,
    startTS: 0,
    frameNo: 0,
    storyWritesText: true,
    balloons: {
      targetBalloons: 0,
      pumps: 0,
      currentColor: 'purple',
      finishedBalloons: 0
    }
  }),
  getters: {
    getCanvasWidth: (state) => (type = 'basic') => {
      if (type === 'basic') {
        return state.basicCanvas.width
      }
      return state.webglCanvas.width
    },
    getCanvasHeight: (state) => (type = 'basic') => {
      if (type === 'basic') {
        return state.basicCanvas.height
      }
      return state.webglCanvas.height
    }
  },
  actions: {
    initCanvas () {
      const savestate = useSavestateStore()
      if (savestate.game.type === 'adventure') {
        this.webglCanvas = document.getElementById('webgl-canvas')
        this.webglContext = this.webglCanvas.getContext('webgl')
        this.basicCanvas = document.getElementById('basic-canvas')
        this.basicContext = this.basicCanvas.getContext('2d')
      } else if (savestate.game.type !== 'noGame') {
        this.basicCanvas = document.getElementById('basic-canvas')
        this.basicContext = this.basicCanvas.getContext('2d')
      }
    },
    shuffleAlphabets () {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()

      const appLangInfo = appConst.targetLanguages[savestate.app.lang]
      const targetLangInfo = appConst.targetLanguages[savestate.app.targetLanguage]

      if (Math.random() < 0.5) {
        if (targetLangInfo.foreignAlphabet === '') {
          this.vocabs.mainAlphabet = targetLangInfo.latinAlphabet
        } else {
          this.vocabs.mainAlphabet = targetLangInfo.foreignAlphabet
        }
        this.vocabs.latinAlphabet = appLangInfo.latinAlphabet
        this.vocabs.foreignAlphabet = appLangInfo.foreignAlphabet
      } else {
        if (appLangInfo.foreignAlphabet === '') {
          this.vocabs.mainAlphabet = appLangInfo.latinAlphabet
        } else {
          this.vocabs.mainAlphabet = appLangInfo.foreignAlphabet
        }
        this.vocabs.latinAlphabet = targetLangInfo.latinAlphabet
        this.vocabs.foreignAlphabet = targetLangInfo.foreignAlphabet
      }
    }
  }
})