import { defineStore } from 'pinia'
import Spritesheet from '@/assets/spritesheet.png'
import SpriteDict from '@/canvas/spritedict.json'

const fragments = import.meta.glob('../canvas/games/adventure/fragments/*.js', { eager: true })
const games = import.meta.glob('../canvas/games/*/index.js', { eager: true })

export const useGameConstStore = defineStore({
  id: 'gameconst',
  state: () => ({
    refreshrate: 1000 / 60,
    games: Object.fromEntries(
      Object.entries(games).map(([key, value]) => {
        let obj = {
          ref: value
        }
        for (let key of Object.keys(value.constants)) {
          obj[key] = value.constants[key]
        }
        return [
          key.split('/')[3],
          obj
        ]
      })
    ),
    fragments: Object.values(fragments),
    spritesheet: new Image(),
    spriteDict: SpriteDict
  }),
  getters: {
  },
  actions: {
    initSpritesheet () {
      this.spritesheet.src = Spritesheet
    }
  }
})