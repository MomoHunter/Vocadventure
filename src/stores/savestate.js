import { defineStore } from 'pinia'
import { useAppConstStore } from './appconst'
import { useAppDynStore } from './appdyn'
import { useIndexedDBStore } from './indexeddb'

export const useSavestateStore = defineStore({
  id: 'savestate',
  state: () => ({
    version: '0.2.2',
    app: {
      lang: 'german',
      targetLanguage: 'japanese',
      theme: 'garium',
      size: 'normal',
      viewport: 1,
      volume: 100,
      status: {
        points: { count: 0, additional: 0 },
        vocabs: { count: 0, additional: 0 },
        streak: { count: 0, additional: 0 }
      },
      allowUpdates: true,
      deactivateWordsAdventure: true,
      missedUpdates: false,
      updateSuccessful: false,
      activeWordpacks: ['s_1', 's_2', 's_3', 's_4'],
      categoriesStats: [], // { key: 's_1_1', count: 1, lastPlayed: 1649786083212, proficiency (last 20): [0.87, 0.56, 1] }
      deactivatedWords: [], // { categoryKey: 's_1_5', index: 23 }
      streakType: 'streakAllDays',
      writeKanjiAnimationSpeed: 25,
      checkLatinAlphabet: false,
      achievementValues: {
        longSession: 0
      }
    },
    game: {
      type: 'balloons',
      balloons: {
        ordersFinished: 0,
        created: {
          purple: 0,
          orange: 0,
          green: 0,
          white: 0,
          black: 0,
          rainbow: 0,
          redpanda: 0,
          yellow: 0,
          pink: 0,
          red: 0,
          blue: 0,
          lime: 0,
          lightblue: 0,
          rainbow2: 0,
          applepie: 0,
          pig: 0,
          gold: 0,
          frog: 0,
          dog: 0
        }
      },
      adventure: {
        storyFragment: 0,
        storyPart: 1,
        gameState: 'test' // TODO: revert to 'story' for correct first startup
      }
    }
  }),
  getters: {
    getCategoryStats: (state) => (key) => {
      const data = state.app.categoriesStats.find(category => category.key === key)
      return data || { key: key, count: 0, lastPlayed: 0, proficiency: [] }
    },
    isWordDeactivated: (state) => (categoryKey, index) => {
      return state.app.deactivatedWords.findIndex((word) =>
        word.categoryKey === categoryKey && word.index === index
      ) !== -1
    },
    isVersionSmaller: (state) => (testVersion, compareVersion) => {
      const testNumbers = testVersion.split('.')
      const compareNumbers = compareVersion.split('.')

      for (let i = 0; i < testNumbers.length; i++) {
        if (parseInt(testNumbers[i]) < parseInt(compareNumbers[i])) {
          return true
        } else if (parseInt(testNumbers[i]) > parseInt(compareNumbers[i])) {
          return false
        }
      }
      return false
    }
  },
  actions: {
    saveData (type = 'all', obj = 'app' ) {
      const savestate = useSavestateStore()
      const indexedDBStore = useIndexedDBStore()

      if (type === 'all') {
        indexedDBStore.saveEntry({
          name: 'vocadventuredb',
          store: 'savestate',
          entry: {
            a: 'version',
            b: this.version
          },
          create: true,
          keyPath: 'a'
        })
        for (let key in this.app) {
          indexedDBStore.saveEntry({
            name: 'vocadventuredb',
            store: 'savestate',
            entry: {
              a: `app.${key}`,
              b: savestate.minifyData(`app.${key}`, this.app[key])
            },
            create: true,
            keyPath: 'a'
          })
        }
        for (let key in this.game) {
          indexedDBStore.saveEntry({
            name: 'vocadventuredb',
            store: 'savestate',
            entry: {
              a: `game.${key}`,
              b: savestate.minifyData(`game.${key}`, this.game[key])
            },
            create: true,
            keyPath: 'a'
          })
        }
      } else {
        indexedDBStore.saveEntry({
          name: 'vocadventuredb',
          store: 'savestate',
          entry: {
            a: `${obj}.${type}`,
            b: savestate.minifyData(`${obj}.${type}`, this[obj][type])
          },
          create: true,
          keyPath: 'a'
        })
      }
    },
    loadData (data) {
      const savestate = useSavestateStore()
      const appDyn = useAppDynStore()

      if (data) {
        if (Object.hasOwn(data, 'version') && data.version !== this.version) {
          data = savestate.convertData(data)
        }

        if (Object.hasOwn(data, 'app')) {
          if (Object.hasOwn(data.app, 'updateSuccessful') && data.app.updateSuccessful) {
            if (navigator.serviceWorker) {
              navigator.serviceWorker.getRegistration().then((registration) => {
                if (registration && registration.waiting) {
                  this.app.updateSuccessful = true
                } else {
                  appDyn.updateSuccess = true
                  savestate.saveData('updateSuccessful')
                }
              })
            }
          }
  
          if (Object.hasOwn(data.app, 'lang') && data.app.lang !== this.app.lang) {
            this.app.lang = data.app.lang
          }
          if (Object.hasOwn(data.app, 'targetLanguage') && data.app.targetLanguage !== this.app.targetLanguage) {
            this.app.targetLanguage = data.app.targetLanguage
          }
          if (Object.hasOwn(data.app, 'theme') && data.app.theme !== this.app.theme) {
            savestate.changeTheme(data.app.theme)
          }
          if (Object.hasOwn(data.app, 'size') && data.app.size !== this.app.size) {
            this.app.size = data.app.size
          }
          if (Object.hasOwn(data.app, 'viewport') && data.app.viewport !== this.app.viewport) {
            savestate.changeViewport(data.app.viewport)
          }
          if (Object.hasOwn(data.app, 'volume') && data.app.volume !== this.app.volume) {
            this.app.volume = data.app.volume
          }
          if (Object.hasOwn(data.app, 'status') && data.app.status !== this.app.status) {
            this.app.status = data.app.status
          }
          if (Object.hasOwn(data.app, 'deactivateWordsAdventure') && data.app.deactivateWordsAdventure !== this.app.deactivateWordsAdventure) {
            this.app.deactivateWordsAdventure = data.app.deactivateWordsAdventure
          }
          if (Object.hasOwn(data.app, 'missedUpdates') && data.app.missedUpdates !== this.app.missedUpdates) {
            this.app.missedUpdates = data.app.missedUpdates
          }
          if (Object.hasOwn(data.app, 'allowUpdates') && data.app.allowUpdates !== this.app.allowUpdates) {
            savestate.changeAllowUpdates(data.app.allowUpdates)
          }
          if (Object.hasOwn(data.app, 'activeWordpacks') && data.app.activeWordpacks !== this.app.activeWordpacks) {
            this.app.activeWordpacks = data.app.activeWordpacks
          }
          if (Object.hasOwn(data.app, 'categoriesStats') && data.app.categoriesStats !== this.app.categoriesStats) {
            this.app.categoriesStats = data.app.categoriesStats
          }
          if (Object.hasOwn(data.app, 'deactivatedWords') && data.app.deactivatedWords !== this.app.deactivatedWords) {
            this.app.deactivatedWords = data.app.deactivatedWords
          }
          if (Object.hasOwn(data.app, 'streakType') && data.app.streakType !== this.app.streakType) {
            this.app.streakType = data.app.streakType
            savestate.changeStreakType(data.app.streakType)
          }
          if (Object.hasOwn(data.app, 'writeKanjiAnimationSpeed') && data.app.writeKanjiAnimationSpeed !== this.app.writeKanjiAnimationSpeed) {
            this.app.writeKanjiAnimationSpeed = data.app.writeKanjiAnimationSpeed
          }
          if (Object.hasOwn(data.app, 'checkLatinAlphabet') && data.app.checkLatinAlphabet !== this.app.checkLatinAlphabet) {
            this.app.checkLatinAlphabet = data.app.checkLatinAlphabet
          }
          if (Object.hasOwn(data.app, 'achievementValues') && data.app.achievementValues !== this.app.achievementValues) {
            this.app.achievementValues = data.app.achievementValues
          }
        }

        if (Object.hasOwn(data, 'game')) {
          if (Object.hasOwn(data.game, 'type') && data.game.type !== this.game.type) {
            this.game.type = data.game.type
          }
          if (Object.hasOwn(data.game, 'balloons') && data.game.balloons !== this.game.balloons) {
            this.game.balloons = data.game.balloons
          }
        }
      }
    },
    deleteData () {
      const indexedDBStore = useIndexedDBStore()
      indexedDBStore.clearObjectStore({
        name: 'vocadventuredb',
        store: 'savestate'
      })
      indexedDBStore.clearObjectStore({
        name: 'vocadventuredb',
        store: 'events'
      })

    },
    convertData (oldData) { // for conversion between versions
      const savestate = useSavestateStore()
      if (savestate.isVersionSmaller(oldData.version, '0.2.2')) {
        oldData.app.status = {
          points: {
            count: oldData.app.status[0].count,
            additional: oldData.app.status[0].additional
          },
          vocabs: {
            count: oldData.app.status[1].count,
            additional: oldData.app.status[1].additional
          },
          streak: {
            count: oldData.app.status[2].count,
            additional: oldData.app.status[2].additional
          }
        }
        oldData.version = '0.2.2'
      }
      return oldData
    },
    minifyData (type, data) {
      const appConst = useAppConstStore()

      switch (type) {
        case 'app.status':
          if (Array.isArray(data)) {
            return data.map(val => {
              return {
                a: val.id,
                b: val.count,
                c: val.additional
              }
            })
          }
          return {
            a: {
              a: data.points.count,
              b: data.points.additional
            },
            b: {
              a: data.vocabs.count,
              b: data.vocabs.additional
            },
            c: {
              a: data.streak.count,
              b: data.streak.additional
            }
          }
        case 'app.categoriesStats':
          return data.map(val => {
            return {
              a: val.key,
              b: val.count,
              c: val.lastPlayed,
              d: JSON.parse(JSON.stringify(val.proficiency))
            }
          })
        case 'app.deactivatedWords':
          return data.map(val => {
            return {
              a: val.categoryKey,
              b: val.index
            }
          })
        case 'app.activeWordpacks':
          return data.map(val => val) // to get rid of the proxy Array
        case 'app.achievementValues':
          return {
            a: data.longSession
          }
        case 'game.balloons':
          return {
            a: {
              a: data.created.purple,
              b: data.created.orange,
              c: data.created.green,
              d: data.created.white,
              e: data.created.black,
              f: data.created.rainbow,
              g: data.created.redpanda,
              h: data.created.yellow,
              i: data.created.pink,
              j: data.created.red,
              k: data.created.blue,
              l: data.created.lime,
              m: data.created.lightblue,
              n: data.created.rainbow2,
              o: data.created.applepie,
              p: data.created.pig,
              q: data.created.gold,
              r: data.created.frog,
              s: data.created.dog
            },
            b: data.ordersFinished
          }
        case 'game.adventure':
          return {
            a: data.storyFragment,
            b: data.storyPart,
            c: data.gameState
          }
        case 'wordpack':
          let packObj = {
            a: data.index,
            b: data.tag,
            c: data.name,
            d: data.targetLanguage,
            e: data.supportedLanguages,
            f: data.isCustom,
            g: data.categories.map(cat => {
              let obj = {
                a: cat.index,
                b: cat.words.map(word => {
                  let wordObj = {
                    a: word.difficulty
                  }
                  let startCode = 98 // b
                  if (Object.hasOwn(word, 'tags')) {
                    wordObj.b = word.tags
                    startCode = 99 // c
                  }

                  let keys = [appConst.targetLanguages[data.targetLanguage].latinAlphabet]
                  if (appConst.targetLanguages[data.targetLanguage].foreignAlphabet !== '') {
                    keys.push(appConst.targetLanguages[data.targetLanguage].foreignAlphabet)
                  }
                  keys = keys.concat(data.supportedLanguages)

                  for (let i = 0; i < keys.length; i++) {
                    wordObj[String.fromCharCode(startCode + i)] = word[keys[i]]
                  }

                  if (Object.hasOwn(word, 'usesPitch')) {
                    wordObj.A = word.usesPitch.map(used => used ? '1' : '0').join('')
                    wordObj.B = word.pitchAccents.map(pitches => pitches.map(up => up ? '1' : '0').join(''))
                  }
                  return wordObj
                })
              }
              for (let i = 0; i < data.supportedLanguages.length; i++) {
                obj[String.fromCharCode(99 + i)] = cat[data.supportedLanguages[i]]
              }
              return obj
            })
          }
          if (Object.hasOwn(data, 'tags')) {
            packObj.h = data.tags.map(tag => {
              let tagObj = {
                a: tag.color
              }
              for (let i = 0; i < data.supportedLanguages.length; i++) {
                tagObj[String.fromCharCode(98 + i)] = tag[data.supportedLanguages[i]]
              }
              return tagObj
            })
          }
          return packObj
        case 'event':
          let eventObj = {
            a: data.type,
            b: data.name,
            c: data.start,
            d: data.end
          }
          if (Object.hasOwn(data, 'stats')) {
            if (data.name === 'adventure') {
              eventObj.e = {
                a: data.stats.points,
                b: data.stats.words,
                c: JSON.parse(JSON.stringify(data.stats.categories))
              }
            } else if (data.name === 'achievement') {
              eventObj.e = {
                d: data.stats.categoryId,
                e: data.stats.id
              }
            }
          }
          return eventObj
        default:
          return data
      }
    },
    unminifyData (type, data) {
      const appConst = useAppConstStore()

      switch (type) {
        case 'app.status':
          if (Array.isArray(data)) {
            return data.map(val => {
              return {
                id: val.a,
                count: val.b,
                additional: val.c
              }
            })
          }
          return {
            points: {
              count: data.a.a,
              additional: data.a.b
            },
            vocabs: {
              count: data.b.a,
              additional: data.b.b
            },
            streak: {
              count: data.c.a,
              additional: data.c.b
            }
          }
        case 'app.categoriesStats':
          return data.map(val => {
            return {
              key: val.a,
              count: val.b,
              lastPlayed: val.c,
              proficiency: val.d
            }
          })
        case 'app.deactivatedWords':
          return data.map(val => {
            return {
              categoryKey: val.a,
              index: val.b
            }
          })
        case 'app.achievementValues':
          return {
            longSession: data.a || 0
          }
        case 'game.balloons':
          return {
            created: {
              purple: data.a?.a || 0,
              orange: data.a?.b || 0,
              green: data.a?.c || 0,
              white: data.a?.d || 0,
              black: data.a?.e || 0,
              rainbow: data.a?.f || 0,
              redpanda: data.a?.g || 0,
              yellow: data.a?.h || 0,
              pink: data.a?.i || 0,
              red: data.a?.j || 0,
              blue: data.a?.k || 0,
              lime: data.a?.l || 0,
              lightblue: data.a?.m || 0,
              rainbow2: data.a?.n || 0,
              applepie: data.a?.o || 0,
              pig: data.a?.p || 0,
              gold: data.a?.q || 0,
              frog: data.a?.r || 0,
              dog: data.a?.s || 0
            },
            ordersFinished: data.b || 0
          }
        case 'game.adventure':
          return {
            storyFragment: data.a,
            storyPart: data.b,
            gameState: data.c
          }
        case 'wordpack':
          let packObj = {
            index: data.a,
            tag: data.b,
            name: data.c,
            targetLanguage: data.d,
            supportedLanguages: data.e,
            isCustom: data.f,
            categories: data.g.map(cat => {
              let catObj = {
                index: cat.a,
                words: cat.b.map(word => {
                  let wordObj = {
                    difficulty: word.a
                  }
                  let startCode = 98
                  let keys = [appConst.targetLanguages[data.d].latinAlphabet]
                  if (appConst.targetLanguages[data.d].foreignAlphabet !== '') {
                    keys.push(appConst.targetLanguages[data.d].foreignAlphabet)
                  }
                  keys = keys.concat(data.e)

                  if (Object.hasOwn(word, String.fromCharCode(98 + keys.length))) {
                    wordObj.tags = word.b
                    startCode = 99
                  }

                  for (let i = 0; i < keys.length; i++) {
                    wordObj[keys[i]] = word[String.fromCharCode(startCode + i)]
                  }

                  if (Object.hasOwn(word, 'A')) {
                    wordObj.usesPitch = word.A.split('').map(used => !!parseInt(used))
                    wordObj.pitchAccents = word.B.map(pitches => pitches.split('').map(up => !!parseInt(up)))
                  }
                  return wordObj
                })
              }
              for (let i = 0; i < data.e.length; i++) {
                catObj[data.e[i]] = cat[String.fromCharCode(99 + i)]
              }
              return catObj
            })
          }
          if (Object.hasOwn(data, 'h')) {
            packObj.tags = data.h.map(tag => {
              let tagObj = {
                color: tag.a
              }
              for (let i = 0; i < data.e.length; i++) {
                tagObj[data.e[i]] = tag[String.fromCharCode(98 + i)]
              }
              return tagObj
            })
          }
          return packObj
        case 'event':
          let eventObj = {
            type: data.a,
            name: data.b,
            start: data.c,
            end: data.d
          }
          if (Object.hasOwn(data, 'e')) {
            if (data.b === 'adventure') {
              eventObj.stats = {
                points: data.e.a,
                words: data.e.b,
                categories: data.e.c
              }
            } else if (data.b === 'achievement') {
              eventObj.stats = {
                categoryId: data.e.d,
                id: data.e.e
              }
            }
          }
          return eventObj
        default:
          return data
      }
    },
    changeTheme (newTheme) {
      const appConst = useAppConstStore()

      this.app.theme = newTheme

      document.getElementById('cssHook').href = import.meta.env.BASE_URL + appConst.themes[newTheme].substring(1)
    },
    changeViewport (newViewport) {
      let viewportTag = document.querySelector('[name~=viewport][content]')

      this.app.viewport = newViewport
      viewportTag.content = `width=device-width, initial-scale=${newViewport}`
    },
    changeStreakType (newStreakType) {
      const appConst = useAppConstStore()
      const addZero = (number) => {
        return number > 9 ? number : '0' + number
      }

      let events = Object.values(appConst.events).map(month => Object.values(month)).flat(2)
      let activeDays = events.reduce((days, entry) => {
        const startDate = new Date(entry.start)
        const day = `${startDate.getFullYear()}${addZero(startDate.getMonth())}${addZero(startDate.getDate())}`

        if (!days.includes(day)) {
          days.push(day)
        }
        return days
      }, [])

      switch (newStreakType) {
        case 'streakSuccessiveDays':
          activeDays.sort((a, b) => parseInt(b) - parseInt(a))
          let currentDate = new Date()
          let dateString = `${currentDate.getFullYear()}${addZero(currentDate.getMonth())}${addZero(currentDate.getDate())}`
          let days = 0
          if (activeDays[0] === dateString) {
            days += 1
          }
          currentDate.setDate(currentDate.getDate() - 1)
          dateString = `${currentDate.getFullYear()}${addZero(currentDate.getMonth())}${addZero(currentDate.getDate())}`
          while (activeDays[days] === dateString) {
            days += 1
            currentDate.setDate(currentDate.getDate() - 1)
            dateString = `${currentDate.getFullYear()}${addZero(currentDate.getMonth())}${addZero(currentDate.getDate())}`
          }

          this.app.status.streak.count = days
          break
        case 'streakAllDays':
          this.app.status.streak.count = activeDays.length
          break
        default:
      }

      this.app.streakType = newStreakType
    },
    changeAllowUpdates (allowUpdates) {
      const appDyn = useAppDynStore()

      if (allowUpdates && this.app.missedUpdates) {
        appDyn.updatesWillInstall = true
      }
      this.app.allowUpdates = allowUpdates
    }
  }
})

/*
Versioning

0.2.0 Basic
0.2.1 Adds app.checkLatinAlphabet and game.type + typesetup in game for balloons and adventure
      Add app.streakType
0.2.2 Add app.achievementValues
      Restructure status
*/