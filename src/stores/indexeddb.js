import { defineStore } from 'pinia'
import { useAppConstStore } from './appconst'
import { useSavestateStore } from './savestate'

export const useIndexedDBStore = defineStore({
  id: 'indexeddb',
  state: () => ({
  }),
  getters: {
  },
  actions: {
    async getDb (data, version = null) {
      return new Promise((resolve, reject) => {
        let request = null
        if (version) {
          request = window.indexedDB.open(data.name, version)
        } else {
          request = window.indexedDB.open(data.name)
        }

        request.onerror = e => {
          console.error('Error opening db', e)
          reject()
        }

        request.onsuccess = e => {
          let db = e.target.result
          if (!db.objectStoreNames.contains(data.store)) {
            if (!data.create) {
              reject()
            } else {
              resolve(this.getDb(data, db.version + 1))
            }
          } else {
            resolve(db)
          }
        }

        request.onupgradeneeded = e => {
          if (data.create) {
            console.log(`New Object store ${data.store} will be created`)
            let db = e.target.result
            if (!!data.autoIncrement) {
              db.createObjectStore(data.store, { autoIncrement: true })
            } else {
              db.createObjectStore(data.store, { autoIncrement: false, keyPath: data.keyPath })
            }
          }
        }
      }).catch(e => {
        return null
      })
    },
    async saveEntry (data) {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      let db = await this.getDb(data)

      await new Promise(resolve => {
        let trans = db.transaction([data.store], 'readwrite')
        trans.oncomplete = () => {
          resolve(data.entry)
        }
        
        let store = trans.objectStore(data.store)
        console.log(data.entry)
        store.put(data.entry)
      }).then(entry => {
        switch (data.store) {
          case 'wordpacks':
            appConst.addWordpack(savestate.unminifyData('wordpack', entry))
            break
          case 'events':
            appConst.addEvent(savestate.unminifyData('event', entry))
            break
          default:
        }
      })
    },
    async getEntries (data) {
      const appConst = useAppConstStore()
      const savestate = useSavestateStore()
      let db = await this.getDb(data)

      if (db === null) {
        return
      }

      await new Promise(resolve => {
        let trans = db.transaction([data.store], 'readonly')
        trans.oncomplete = () => {
          resolve(loadedInfo)
        }

        let store = trans.objectStore(data.store)
        let loadedInfo = []

        store.openCursor().onsuccess = e => {
          let cursor = e.target.result
          if (cursor) {
            loadedInfo.push(cursor.value)
            cursor.continue()
          }
        }
      }).then(loadedInfo => {
        switch (data.store) {
          case 'wordpacks':
            loadedInfo.forEach(wordpack => {
              appConst.addWordpack(savestate.unminifyData('wordpack', wordpack))
            })
            break
          case 'savestate':
            let convertedData = {}
            for (let setting of loadedInfo) {
              const keys = setting.a.split('.')
              let currentObj = convertedData
              for (let key of keys) {
                if (key === keys[keys.length - 1]) {
                  currentObj[key] = savestate.unminifyData(setting.a, setting.b)
                } else if (Object.hasOwn(currentObj, key)) {
                  currentObj = currentObj[key]
                } else {
                  currentObj[key] = {}
                  currentObj = currentObj[key]
                }
              }
            }
            savestate.loadData(convertedData)
            break
          case 'events':
            loadedInfo.forEach(entry => {
              appConst.addEvent(savestate.unminifyData('event', entry))
            })
            savestate.changeStreakType(savestate.app.streakType)
            break
          default:
        }
      })
    },
    async deleteEntry (data) {
      const appConst = useAppConstStore()
      let db = await this.getDb(data)

      await new Promise(resolve => {
        let trans = db.transaction([data.store], 'readwrite')
        trans.oncomplete = () => {
          resolve(data.key)
        }

        let store = trans.objectStore(data.store)
        switch (data.store) {
          case 'wordpacks':
            store.delete(parseInt(data.key.split('_')[1]))
            break
          case 'savestate':
            store.delete(data.key)
            break
          case 'events':
            store.delete(data.key)
            break
          default:
        }
      }).then(key => {
        switch (data.store) {
          case 'wordpacks':
            appConst.deleteWordpack(key)
            break
          case 'savestate':
            if (key === 'app.writeKanjiAnimationSpeed') {
              setTimeout(() => location.reload(), 300)
            }
            break
          default:
        }
      })
    },
    async clearObjectStore (data) {
      const appConst = useAppConstStore()
      let db = await this.getDb(data)

      await new Promise(resolve => {
        let trans = db.transaction([data.store], 'readwrite')
        trans.oncomplete = () => {
          resolve(data.store)
        }

        let store = trans.objectStore(data.store)
        store.clear()
      }).then(store => {
        switch (store) {
          case 'savestate':
            setTimeout(() => location.reload(), 300)
            break
          case 'events':
            appConst.events = {}
            break
          default:
        }
      })
    },
    async deleteObjectStore (data, version = null) {
      await new Promise((resolve, reject) => {
        let request = null
        if (version) {
          request = window.indexedDB.open(data.name, version)
        } else {
          request = window.indexedDB.open(data.name)
        }

        request.onerror = e => {
          console.error('Error opening db', e)
          reject(e)
        }

        request.onsuccess = e => {
          let db = e.target.result
          if (db.objectStoreNames.contains(data.store)) {
            resolve(this.deleteObjectStore(data, db.version + 1))
          } else {
            resolve()
          }
        }

        request.onupgradeneeded = e => {
          let db = e.target.result
          db.deleteObjectStore(data.store)
          console.log(`Object store ${data.store} was deleted!`)
          location.reload()
        }
      })
    },
    async deleteDB (data) {
      await new Promise((resolve, reject) => {
        let request = window.indexedDB.deleteDatabase(data.name)

        request.onerror = e => {
          console.error('Error deleting db', e)
          reject()
        }

        request.onsuccess = (event) => {
          console.log('Database successfully deleted!')
          resolve()
        }
      })
    }
  }
})