import { defineStore } from 'pinia'
import { useSavestateStore } from './savestate'
import { useAppDynStore } from './appdyn'

import TargetLanguages from '@/data/TargetLanguages.js'
import Themes from '@/data/Themes.json'

const textObj = import.meta.glob('../data/translations/*.json', { eager: true })
const vocabs = import.meta.glob('../data/wordpacks/*.json', { eager: true })

export const useAppConstStore = defineStore({
  id: 'appconst',
  state: () => ({
    texts: Object.fromEntries(
      Object.entries(textObj).map(([key, value]) => {
        return [
          key.substring(
            key.lastIndexOf('/') + 1,
            key.lastIndexOf('.')
          ),
          value
        ]
      })
    ),
    targetLanguages: TargetLanguages,
    tagColors: [
      'green',
      'yellow',
      'orange',
      'red',
      'pink',
      'violet',
      'info',
      'action',
      'selected',
      'weapons',
      'armor',
      'gold',
      'silver',
      'consumables'
    ],
    sizeClasses: {
      general: {
        tiny: 'is-tiny',
        small: 'is-small',
        normal: 'is-normal',
        large: 'is-large',
        giga: 'is-giga'
      },
      style: {
        tiny: 0.7,
        small: 0.8,
        normal: 1,
        large: 1.4,
        giga: 2
      }
    },
    themes: Themes,
    vocabulary: Object.values(vocabs),
    kanaSvg: null,
    achievements: [
      {
        id: 'balloonsAmounts',
        color: 'pink',
        icon: 'chart-simple',
        game: 'balloons',
        entries: [
          {
            id: 'allTheBalloons',
            maxValue: 1_000,
            unlock: {
              type: 'add',
              config: {},
              parts: [
                ['savestate', 'game', 'balloons', 'created', '*']
              ]
            },
            xp: 1_000
          },
          {
            id: 'allTheOrders',
            maxValue: 100,
            unlock: {
              type: 'add',
              config: {},
              parts: [
                ['savestate', 'game', 'balloons', 'ordersFinished']
              ]
            },
            xp: 500
          },
          {
            id: 'rainbowBalloons',
            maxValue: 9,
            unlock: {
              type: 'max',
              config: {
                maxValue: 1
              },
              parts: [
                ['savestate', 'game', 'balloons', 'created', 'purple'],
                ['savestate', 'game', 'balloons', 'created', 'orange'],
                ['savestate', 'game', 'balloons', 'created', 'green'],
                ['savestate', 'game', 'balloons', 'created', 'yellow'],
                ['savestate', 'game', 'balloons', 'created', 'pink'],
                ['savestate', 'game', 'balloons', 'created', 'red'],
                ['savestate', 'game', 'balloons', 'created', 'blue'],
                ['savestate', 'game', 'balloons', 'created', 'lime'],
                ['savestate', 'game', 'balloons', 'created', 'lightblue']
              ]
            },
            xp: 100
          }
        ]
      },
      {
        id: 'balloonsRare',
        color: 'gold',
        icon: 'crown',
        game: 'balloons',
        entries: [
          {
            id: 'everyAnimal',
            maxValue: 6,
            unlock: {
              type: 'max',
              config: {
                maxValue: 1
              },
              parts: [
                ['savestate', 'game', 'balloons', 'created', 'redpanda'],
                ['savestate', 'game', 'balloons', 'created', 'pig'],
                ['savestate', 'game', 'balloons', 'created', 'frog'],
                ['savestate', 'game', 'balloons', 'created', 'dog']
              ]
            },
            xp: 5_555
          }
        ]
      },
      {
        id: 'story',
        color: 'consumables',
        icon: 'wrench',
        game: 'adventure',
        entries: [
          {
            id: 'startGame',
            maxValue: 1,
            unlock: {
              type: 'add',
              config: {},
              parts: []
            },
            xp: 100
          }
        ]
      },
      {
        id: 'general',
        color: 'armor',
        icon: 'eye',
        game: 'adventure',
        entries: [
          {
            id: 'randomAck',
            maxValue: 50000,
            unlock: {
              type: 'add',
              config: {},
              parts: []
            },
            xp: 45000
          }
        ]
      },
      {
        id: 'vocabs',
        color: 'orange',
        icon: 'list-check',
        game: '*',
        entries: [
          {
            id: 'longSession',
            maxValue: 100,
            unlock: {
              type: 'highest',
              config: {},
              parts: [
                ['appDyn', 'wordCount'],
                ['savestate', 'app', 'achievementValues', 'longSession']
              ]
            },
            xp: 500
          }
        ]
      }
    ],
    events: {} // { 2024: { 5: [ { type, name, start, end, stats? } ] } }
  }),
  getters: {
    /**
     * automatically translates text with a text id
     * @param {Object} state state of pinia store component
     * @param {String} id the text id
     * @param {Any} params parameters that should get inserted into the text at & positions
     */
    getText: (state) => (id, ...params) => {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      if (isNaN(id)) {
        let text = state.texts[savestate.app.lang][id]
        if (text) {
          if (params.length > 0) {
            for (let i = 0; i < params.length; i++) {
              let regex = new RegExp('&' + (i + 1), 'g')
              text = text.replace(regex, appConst.getText(params[i]))
            }
          }
          return text
        } else {
          return id
        }
      } else {
        return id.toString()
      }
    },
    /**
     * Returns the size class for the globally selected size
     * @param {Object} state state of pinia store component
     * @param {String} type the type of the size
     */
    getSizeClass: (state) => (type) => {
      const savestate = useSavestateStore()
      return state.sizeClasses[type][savestate.app.size]
    },
    getWordpackKey: (state) => (wordpack) => {
      return (wordpack.isCustom ? 'c' : 's') + '_' + wordpack.index.toString()
    },
    getWordpackTag: (state) => (key) => {
      const keyParts = key.split('_')
      const foundWordpack = state.vocabulary.find(wordpack =>
        ((!wordpack.isCustom && keyParts[0] === 's') || (wordpack.isCustom && keyParts[0] === 'c')) &&
        wordpack.index === parseInt(keyParts[1])
      )
      if (foundWordpack) {
        return foundWordpack.tag
      }
      return ''
    },
    getNewWordpackKey: (state) => {
      return 'c_' + state.vocabulary.reduce((maxIndex, wordpack) => {
        if (wordpack.isCustom) {
          return wordpack.index >= maxIndex ? wordpack.index + 1 : maxIndex
        }
        return maxIndex
      }, 1).toString()
    },
    getWordpack: (state) => (key, copy = true) => {
      const keyParts = key.split('_')
      const foundWordpack = state.vocabulary.find(wordpack =>
        ((!wordpack.isCustom && keyParts[0] === 's') || (wordpack.isCustom && keyParts[0] === 'c')) &&
        wordpack.index === parseInt(keyParts[1])
      )
      if (foundWordpack) {
        if (copy) {
          return JSON.parse(JSON.stringify(foundWordpack))
        }
        return foundWordpack
      }
      return null
    },
    getCategory: (state) => (key, copy = true) => {
      const appConst = useAppConstStore()
      const keyParts = key.split('_')
      const wordpack = appConst.getWordpack(`${keyParts[0]}_${keyParts[1]}`, false)
      
      if (!wordpack) {
        return null
      }
      const foundCategory = wordpack.categories.find(category => {
        return category.index === parseInt(keyParts[2])
      })
      if (foundCategory) {
        if (copy) {
          return JSON.parse(JSON.stringify(foundCategory))
        }
        return foundCategory
      }
      return null
    },
    getWord: (state) => (key, copy = true) => {
      const appConst = useAppConstStore()
      const keyParts = key.split('_')
      const category = appConst.getCategory(keyParts.slice(0, 3).join('_'), false)

      if (!category) {
        return null
      }
      const foundWord = category.words[parseInt(keyParts[3])]
      if (foundWord) {
        if (copy) {
          return JSON.parse(JSON.stringify(foundWord))
        }
        return foundWord
      }
      return null
    },
    /**
     * Collects the available categories and evaluates the correct name for the currently selected language
     * @param {Object} state state of pinia store component
     * @returns Array with objects {key, name} where key consists of
     *          if pack is custom, then c, else s (for standard)
     *          index of pack
     *          index of category
     *          the above is separated by _
     */
    getAvailableCategories: (state) => {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      const supportedPacks = state.vocabulary.filter(wordpack => {
        return wordpack.targetLanguage === savestate.app.targetLanguage &&
          savestate.app.activeWordpacks.includes(appConst.getWordpackKey(wordpack)) &&
          wordpack.supportedLanguages.includes(savestate.app.lang)
      })

      return supportedPacks.flatMap(wordpack => {
        let keyStart = appConst.getWordpackKey(wordpack) + '_'
        let categoryName = '[' + wordpack.tag + '] '

        return wordpack.categories.map(category => {
          return {
            key: keyStart + category.index,
            name: categoryName + category[savestate.app.lang]
          }
        })
      })
    },
    /**
     * Searches for the name of a category
     * @param {Object} state state of pinia store component
     * @param {String} key the key of the category, definition can be found by 'getCategories' under 'returns'
     */
    getCategoryName: (state) => (key) => {
      const appConst = useAppConstStore()
      const categories = appConst.getAvailableCategories
      const foundCategory = categories.find((category) => category.key === key)

      if (foundCategory) {
        return foundCategory.name
      }
      return key
    },
    /**
     * Evaluates the average difficulty of a specified category
     * @param {Object} state state of pinia store component
     * @param {String} key the key of the category, definition can be found by 'getCategories' under 'returns'
     */
    getCategoryDifficulty: (state) => (key) => {
      const appConst = useAppConstStore()
      const category = appConst.getCategory(key, false)

      return category.words.reduce((acc, word) => {
        return acc + word.difficulty
      }, 0) / category.words.length
    },
    /**
     * Collects all words included in the chosen categories that are at most the chosen difficulty
     * @param {Object} state state of pinia store component
     * @param {Boolean} difficultyFilter specifies if all words should be added or just based on difficulty
     * @param {Boolean} withEasier specifies if only the chosen difficulty should be used or everything below too
     * @param {Boolean} tagFilter specifies if only words with selected tags and words without banned tags should be selected
     * @param {Boolean} checkDeactivated checks if the word has been deactivated and removes it from the selection
     * @returns a copy of all the words of the chosen categories that are at most the chosen difficulty
     */
    getFullVocabs: (state) => (difficultyFilter = true, withEasier = true, tagFilter = false, checkDeactivated = false) => {
      const savestate = useSavestateStore()
      const appDyn = useAppDynStore()
      const appConst = useAppConstStore()
      const targetLangInfo = state.targetLanguages[savestate.app.targetLanguage]
      const appLangInfo = state.targetLanguages[savestate.app.lang]
      const tagsSelected = appDyn.tagsSelected.obj
      const tagsBanned = appDyn.tagsBanned.obj
      let wordObjects = []

      for (let categoryKey of appDyn.categoriesChosen) {
        const category = appConst.getCategory(categoryKey)
        const wordpackKey = categoryKey.split('_').slice(0, 2).join('_')
        const wordpack = appConst.getWordpack(wordpackKey)

        for (let i = 0; i < category.words.length; i++) {
          if (checkDeactivated && savestate.isWordDeactivated(categoryKey, i)) {
            continue
          }
          
          if (!difficultyFilter || (withEasier && category.words[i].difficulty <= appDyn.wordDifficulty) ||
              (!withEasier && category.words[i].difficulty === appDyn.wordDifficulty)) {
            if (tagFilter) {
              if (!Object.hasOwn(category.words[i], 'tags') && !tagsSelected.includes(-1)) {
                continue
              } else if (Object.hasOwn(category.words[i], 'tags')) {
                let isIncluded = tagsSelected.length === 0
                let hasBanned = false
                for (let tagIndex of category.words[i].tags) {
                  const tagObj = wordpack.tags[tagIndex]

                  for (let tag of tagsSelected) {
                    if (tag === -1) {
                      continue
                    }

                    let isSame = true
                    for (let key in tagObj) {
                      if (tagObj[key] !== tag[key]) {
                        isSame = false
                        break
                      }
                    }
                    if (isSame) {
                      isIncluded = true
                      break
                    }
                  }
                  for (let tag of tagsBanned) {
                    let isSame = true
                    for (let key in tagObj) {
                      if (tagObj[key] !== tag[key]) {
                        isSame = false
                        break
                      }
                    }
                    if (isSame) {
                      hasBanned = true
                      break
                    }
                  }
                }
                if (!isIncluded || hasBanned) {
                  continue
                }
              }
            }
            let wordCopy = JSON.parse(JSON.stringify(category.words[i]))
            if (savestate.app.deactivateWordsAdventure) {
              const foundWord = savestate.app.deactivatedWords.some(
                dWord => dWord.categoryKey === categoryKey && dWord.index === i
              )
              if (foundWord) {
                continue
              }
            }
            // categoryKey and index are for the training and kanji write page to filter unwanted words out
            wordCopy.categoryKey = categoryKey
            wordCopy.index = i
            if (Object.hasOwn(wordCopy, targetLangInfo.latinAlphabet) &&
                !Array.isArray(wordCopy[targetLangInfo.latinAlphabet])) {
              wordCopy[targetLangInfo.latinAlphabet] = [wordCopy[targetLangInfo.latinAlphabet]]
            }
            if (Object.hasOwn(wordCopy, targetLangInfo.foreignAlphabet) &&
                !Array.isArray(wordCopy[targetLangInfo.foreignAlphabet])) {
              wordCopy[targetLangInfo.foreignAlphabet] = [wordCopy[targetLangInfo.foreignAlphabet]]
            }
            if (Object.hasOwn(wordCopy, appLangInfo.latinAlphabet) &&
                !Array.isArray(wordCopy[appLangInfo.latinAlphabet])) {
              wordCopy[appLangInfo.latinAlphabet] = [wordCopy[appLangInfo.latinAlphabet]]
            }
            if (Object.hasOwn(wordCopy, appLangInfo.foreignAlphabet) &&
                !Array.isArray(wordCopy[appLangInfo.foreignAlphabet])) {
              wordCopy[appLangInfo.foreignAlphabet] = [wordCopy[appLangInfo.foreignAlphabet]]
            }
            wordObjects.push(wordCopy)
          }
        }
      }

      let langs = {
        mainAlphabet: appLangInfo.foreignAlphabet === '' ? appLangInfo.latinAlphabet : appLangInfo.foreignAlphabet,
        latinAlphabet: targetLangInfo.latinAlphabet,
        foreignAlphabet: targetLangInfo.foreignAlphabet
      }

      if (appDyn.reversedType === 1 || (appDyn.reversedType === 2 && Math.random() < 0.5)) {
        if (targetLangInfo.foreignAlphabet === '') {
          langs.mainAlphabet = targetLangInfo.latinAlphabet
        } else {
          langs.mainAlphabet = targetLangInfo.foreignAlphabet
        }
        langs.latinAlphabet = appLangInfo.latinAlphabet
        langs.foreignAlphabet = appLangInfo.foreignAlphabet
      }

      return {
        words: wordObjects,
        signs: JSON.parse(JSON.stringify(targetLangInfo.signs)),
        ...langs,
        // lang for tts
        lang: targetLangInfo.lang
      }
    },
    getSelectedTags: (state) => {
      const appDyn = useAppDynStore()
      const appConst = useAppConstStore()
      const vocabs = appConst.getFullVocabs(true, appDyn.includesEasier)
      let tags = []

      for (let word of vocabs.words) {
        if (Object.hasOwn(word, 'tags')) {
          const keyParts = word.categoryKey.split('_')
          const wordpackKey = `${keyParts[0]}_${keyParts[1]}`
          const wordpack = appConst.getWordpack(wordpackKey, false)
          for (let wordIndex of word.tags) {
            const wordTag = wordpack.tags[wordIndex]
            let isInTags = false
            for (let listTag of tags) {
              let isSame = true
              for (let key in wordTag) {
                if (wordTag[key] !== listTag[key]) {
                  isSame = false
                  break
                }
              }
              if (isSame && Object.values(listTag).length === Object.values(wordTag).length) {
                isInTags = true
                break
              }
            }
            if (!isInTags) {
              // wordTag.wordpackKey = wordpackKey
              // wordTag.index = wordIndex
              tags.push(wordTag)
            }
          }
        }
      }

      return tags
    },
    getSearchedWords: (state) => (type, searchString) => {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      let foundWords = []
      state.vocabulary.forEach((wordpack) => {
        if (wordpack.targetLanguage === savestate.app.targetLanguage &&
            savestate.app.activeWordpacks.includes(appConst.getWordpackKey(wordpack))) {
          wordpack.categories.forEach((category) => {
            category.words.forEach((word, index) => {
              let regex = new RegExp('.*' + searchString.toLowerCase().split('').join('.{0,3}') + '.*')
              if (word[type] && word[savestate.app.lang]) {
                if (typeof word[type] === 'string') {
                  if (regex.test(word[type].toLowerCase())) {
                    foundWords.push({
                      word: JSON.parse(JSON.stringify(word)),
                      key: `${appConst.getWordpackKey(wordpack)}_${category.index}_${index}`,
                      weight: appConst.getLevenshteinDistance(searchString.toLowerCase(), word[type].toLowerCase())
                    })
                  }
                } else {
                  let weight = -1
                  word[type].forEach((wordOption) => {
                    if (regex.test(wordOption.toLowerCase())) {
                      let newWeight = appConst.getLevenshteinDistance(searchString.toLowerCase(), wordOption.toLowerCase())
                      if (weight < newWeight) {
                        weight = newWeight
                      }
                    }
                  })
                  if (weight >= 0) {
                    foundWords.push({
                      word: JSON.parse(JSON.stringify(word)),
                      key: `${appConst.getWordpackKey(wordpack)}_${category.index}_${index}`,
                      weight: weight
                    })
                  }
                }
              }
            })
          })
        }
      })

      return foundWords.sort((a, b) => {
        if (a.weight > b.weight) {
          return 1
        } else if (a.weight < b.weight) {
          return -1
        }
        return 0
      })
    },
    getLevenshteinDistance: (state) => (word1, word2) => {
      let d = new Array(word1.length + 1).fill(0).map(() => new Array(word2.length + 1).fill(0))

      for (let i = 0; i <= word1.length; i++) {
        d[i][0] = i
      }
      for (let j = 0; j <= word2.length; j++) {
        d[0][j] = j
      }

      for (let i = 1; i <= word1.length; i++) {
        for (let j = 1; j <= word2.length; j++) {
          let cost = 0
          if (word1[i] !== word2[j]) {
            cost = 5
          }

          // this is where the magic happens
          d[i][j] = Math.min(d[i - 1][j] + 5, d[i][j - 1] + 1, d[i - 1][j - 1] + cost)

          if (i > 1 && j > 1 && word1[i] === word2[j - 1] && word1[i - 1] === word2[j]) {
            d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 0.5)
          }
        }
      }

      return d[word1.length][word2.length]
    },
    getAchievement: (state) => (categoryId, id) => {
      const category = state.achievements.find(cat => cat.id === categoryId)
      if (!category) {
        return null
      }

      return category.entries.find(entry => entry.id === id)
    },
    getAchievementMaxValue: (state) => (categoryId, id) => {
      const category = state.achievements.find(cat => cat.id === categoryId)
      if (!category) {
        return 0
      }

      const achievement = category.entries.find(entry => entry.id === id)
      if (!achievement) {
        return 0
      }

      return achievement.maxValue
    },
    getAchievementValue: (state) => (categoryId, id) => {
      const category = state.achievements.find(cat => cat.id === categoryId)
      if (!category) {
        return 0
      }

      const achievement = category.entries.find(entry => entry.id === id)
      if (!achievement) {
        return 0
      }

      let partValues = []
      for (let i = 0; i < achievement.unlock.parts.length; i++) {
        const part = achievement.unlock.parts[i]
        let store = null
        switch (part[0]) {
          case 'savestate':
            store = useSavestateStore()
            break
          case 'appDyn':
            store = useAppDynStore()
            break
          default:
        }

        const keys = part.slice(1)
        let values = [store]
        let starIndices = []
        for (let j = 0; j <= keys.length; j++) {
          if (keys.length === j) {
            if (keys[j - 1] === '*') {
              const starValues = Object.values(values[j - 1])
              partValues.push(starValues[values[j]])
              j -= 2
              continue
            } else {
              partValues.push(values[j])
              if (starIndices.length > 0) {
                j = starIndices[starIndices.length - 1] - 1
              }
              break
            }
          }

          const key = keys[j]
          
          if (key === '*') {
            const starValues = Object.values(values[j])
            if (values.length === j + 1) {
              if (j === keys.length - 1) {
                values[j + 1] = 0
              } else {
                values[j + 1] = starValues[0]
              }
              starIndices.push(j)
            } else {
              if (j === keys.length - 1) {
                values[j + 1] += 1
                if (values[j + 1] === starValues.length) {
                  starIndices.splice(starIndices.length - 1, 1)
                  if (starIndices.length > 0) {
                    j = starIndices[starIndices.length - 1] - 1
                  } else {
                    break
                  }
                }
              } else {
                const starIndex = starValues.findIndex(val => val === values[j + 1])
                if (starIndex + 1 < starValues.length) {
                  values[j + 1] = starValues[starIndex + 1]
                } else {
                  starIndices.splice(starIndices.length - 1, 1)
                  if (starIndices.length > 0) {
                    j = starIndices[starIndices.length - 1] - 1
                  } else {
                    break
                  }
                }
              }
            }
          } else if (Object.hasOwn(values[j], key)) {
            values[j + 1] = values[j][key]
          }
        }
      }

      let finalValue = 0
      if (['highest'].includes(achievement.unlock.type)) {
        switch (achievement.unlock.type) {
          case 'highest':
            finalValue = Math.max(...partValues)
            break
          default:
        }
      } else {
        for (let partValue of partValues) {
          switch (achievement.unlock.type) {
            case 'add':
              finalValue += partValue
              break
            case 'max':
              finalValue += Math.min(partValue, achievement.unlock.config.maxValue)
              break
            default:
          }
        }
      }

      return Math.min(finalValue, achievement.maxValue)
    }
  },
  actions: {
    addWordpack: (wordpack) => {
      const appConst = useAppConstStore()
      const appDyn = useAppDynStore()

      appConst.vocabulary.push(wordpack)
      appConst.vocabulary.sort((wordpack1, wordpack2) => {
        if (wordpack1.isCustom && !wordpack2.isCustom) {
          return 1
        } else if (!wordpack1.isCustom && wordpack2.isCustom) {
          return -1
        } else {
          if (wordpack1.index > wordpack2.index) {
            return 1
          } else if (wordpack1.index < wordpack2.index) {
            return -1
          } else {
            return 0
          }
        }
      })
      appDyn.packages.saved.db = false
    },
    deleteWordpack: (key, deactivateWords = true) => {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      const keyParts = key.split('_')
      appConst.vocabulary = appConst.vocabulary.filter(wordpack =>
        !(((!wordpack.isCustom && keyParts[0] === 's') || (wordpack.isCustom && keyParts[0] === 'c')) &&
        wordpack.index === parseInt(keyParts[1]))
      )
      if (deactivateWords) {
        savestate.app.deactivatedWords = savestate.app.deactivatedWords.filter((word) => !word.categoryKey.startsWith(key))
      }
      savestate.saveData('deactivatedWords')
    },
    addEvent: (event) => {
      const appConst = useAppConstStore()
      const startDate = new Date(event.start)
      const endDate = new Date(event.end || event.start)
      
      const distYear = endDate.getFullYear() - startDate.getFullYear()
      const distMonth = endDate.getMonth() + (12 - startDate.getMonth()) + (distYear - 1) * 12

      if (distMonth === 0) {
        const yearKey = startDate.getFullYear()
        const monthKey = startDate.getMonth()
        if (!Object.hasOwn(appConst.events, yearKey)) {
          appConst.events[yearKey] = {}
        }
        if (!Object.hasOwn(appConst.events[yearKey], monthKey)) {
          appConst.events[yearKey][monthKey] = []
        }
        appConst.events[yearKey][monthKey].push(event)
      } else {
        for (let i = startDate.getMonth(); i < startDate.getMonth() + distMonth; i++) {
          const yearKey = startDate.getFullYear() + Math.floor(i / 12)
          const monthKey = i % 12
          if (!Object.hasOwn(appConst.events, yearKey)) {
            appConst.events[yearKey] = {}
          }
          if (!Object.hasOwn(appConst.events[yearKey], monthKey)) {
            appConst.events[yearKey][monthKey] = []
          }
          appConst.events[yearKey][monthKey].push(event)
        }
      }
    }
  }
})