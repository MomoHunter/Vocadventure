import * as BasicHelper from '@/canvas/basic-helper.js'
import { savestate, appConst, gameDyn } from '@/canvas/storeref.js'

export const constants = {}

export function init () {}

export function canvasClear () {
  gameDyn.basicContext.clearRect(0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight())
}

export function canvasUpdate () {}

export function canvasDraw () {
  if (savestate.app.theme === 'garium') {
    BasicHelper.drawCanvasText(
      gameDyn.basicContext, 'Adventure coming soon...', 300, 150, 'center', 'middle', 'Nunito', 24, true, [255, 255, 255]
    )
  } else {
    BasicHelper.drawCanvasText(
      gameDyn.basicContext, 'Adventure coming soon...', 300, 150, 'center', 'middle', 'Nunito', 24, true, [0, 0, 0]
    )
  }
}

export function getSaveData () {
  return {}
}

export function reset () {
  
}