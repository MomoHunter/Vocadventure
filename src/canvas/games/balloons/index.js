import * as BasicHelper from '@/canvas/basic-helper.js'
import { appDyn, appConst, gameDyn, gameConst } from '@/canvas/storeref.js'

const data = {
  clipboardPosition: {
    x: 0,
    y: 0
  },
  balloonPosition: {
    x: 300,
    y: 240
  },
  balloonScale: 0.5,
  currentPumps: 0,
  currentName: 0,
  currentEvent: 1,
  finishedBalloonObj: [],
  animation: {
    counter: 0,
    counter2: 0,
    step: 1
  },
  currentColor: 'purple',
  additionalData: {},
  created: {},
  ordersFinished: 0,
  switchOrder: false
}

export const constants = {
  colors: {
    purple: {
      probability: 1,
      main: [137, 29, 175],
      foot: [96, 20, 93]
    }, 
    orange: {
      probability: 1,
      main: [239, 138, 7],
      foot: [178, 102, 3]
    },
    green: {
      probability: 1,
      main: [46, 130, 27],
      foot: [19, 86, 4]
    },
    white: {
      probability: 0.75,
      main: [230, 230, 230],
      foot: [190, 190, 190]
    },
    black: {
      probability: 0.75,
      main: [30, 30, 30],
      foot: [5, 5, 5]
    },
    rainbow: {
      probability: 0.4,
      main: {
        start: {
          x: -1.48,
          y: -0.73
        },
        end: {
          x: 1.08,
          y: 1.08
        },
        stops: [
          { offset: 0.15, color: [196, 69, 69] },
          { offset: 0.25, color: [255, 167, 73] },
          { offset: 0.434, color: [253, 217, 82] },
          { offset: 0.582, color: [107, 225, 108] },
          { offset: 0.751, color: [107, 169, 225] },
          { offset: 0.873, color: [153, 41, 243] }
        ]
      },
      foot: [107, 29, 170]
    },
    redpanda: {
      probability: 0.025,
      white: [240, 240, 240],
      snout: [35, 12, 0],
      main: [255, 117, 38],
      foot: [178, 82, 27]
    },
    yellow: {
      probability: 1,
      main: [234, 214, 65],
      foot: [164, 150, 45]
    },
    pink: {
      probability: 1,
      main: [232, 60, 177],
      foot: [153, 41, 117]
    },
    red: {
      probability: 1,
      main: [226, 54, 57],
      foot: [158, 38, 38]
    },
    blue: {
      probability: 1,
      main: [54, 57, 226],
      foot: [38, 38, 158]
    },
    lime: {
      probability: 1,
      main: [101, 204, 50],
      foot: [70, 141, 35]
    },
    lightblue: {
      probability: 1,
      main: [46, 209, 209],
      foot: [32, 146, 146]
    },
    rainbow2: {
      probability: 0.4,
      main: {
        start: {
          x: 0.8,
          y: -0.7
        },
        end: {
          x: -1,
          y: 0.9
        },
        stops: [
          { offset: 0.129, color: [255, 26, 1] },
          { offset: 0.24, color: [254, 155, 1] },
          { offset: 0.395, color: [255, 241, 0] },
          { offset: 0.55, color: [34, 218, 1] },
          { offset: 0.71, color: [0, 141, 254] },
          { offset: 0.91, color: [113, 63, 254] }
        ]
      },
      foot: [79, 44, 178]
    },
    applepie: {
      probability: 0.15,
      background: [198, 85, 25],
      main: {
        start: {
          x: 1,
          y: -0.4
        },
        end: {
          x: -1,
          y: 0.4
        },
        stops: [
          { offset: 0.06, color: [247, 178, 49] },
          { offset: 0.065, color: 'transparent' },
          { offset: 0.1225, color: 'transparent' },
          { offset: 0.1275, color: [247, 178, 49] },
          { offset: 0.185, color: [247, 178, 49] },
          { offset: 0.19, color: 'transparent' },
          { offset: 0.2475, color: 'transparent' },
          { offset: 0.2525, color: [247, 178, 49] },
          { offset: 0.31, color: [247, 178, 49] },
          { offset: 0.315, color: 'transparent' },
          { offset: 0.3725, color: 'transparent' },
          { offset: 0.3775, color: [247, 178, 49] },
          { offset: 0.435, color: [247, 178, 49] },
          { offset: 0.44, color: 'transparent' },
          { offset: 0.4975, color: 'transparent' },
          { offset: 0.5025, color: [247, 178, 49] },
          { offset: 0.56, color: [247, 178, 49] },
          { offset: 0.565, color: 'transparent' },
          { offset: 0.6225, color: 'transparent' },
          { offset: 0.6275, color: [247, 178, 49] },
          { offset: 0.685, color: [247, 178, 49] },
          { offset: 0.69, color: 'transparent' },
          { offset: 0.7475, color: 'transparent' },
          { offset: 0.7525, color: [247, 178, 49] },
          { offset: 0.81, color: [247, 178, 49] },
          { offset: 0.815, color: 'transparent' },
          { offset: 0.8725, color: 'transparent' },
          { offset: 0.8775, color: [247, 178, 49] },
          { offset: 0.935, color: [247, 178, 49] },
          { offset: 0.94, color: 'transparent' },
        ]
      },
      mainneg: {
        start: {
          x: -1,
          y: -0.4
        },
        end: {
          x: 1,
          y: 0.4
        },
        stops: [
          { offset: 0.06, color: [247, 178, 49] },
          { offset: 0.065, color: 'transparent' },
          { offset: 0.1225, color: 'transparent' },
          { offset: 0.1275, color: [247, 178, 49] },
          { offset: 0.185, color: [247, 178, 49] },
          { offset: 0.19, color: 'transparent' },
          { offset: 0.2475, color: 'transparent' },
          { offset: 0.2525, color: [247, 178, 49] },
          { offset: 0.31, color: [247, 178, 49] },
          { offset: 0.315, color: 'transparent' },
          { offset: 0.3725, color: 'transparent' },
          { offset: 0.3775, color: [247, 178, 49] },
          { offset: 0.435, color: [247, 178, 49] },
          { offset: 0.44, color: 'transparent' },
          { offset: 0.4975, color: 'transparent' },
          { offset: 0.5025, color: [247, 178, 49] },
          { offset: 0.56, color: [247, 178, 49] },
          { offset: 0.565, color: 'transparent' },
          { offset: 0.6225, color: 'transparent' },
          { offset: 0.6275, color: [247, 178, 49] },
          { offset: 0.685, color: [247, 178, 49] },
          { offset: 0.69, color: 'transparent' },
          { offset: 0.7475, color: 'transparent' },
          { offset: 0.7525, color: [247, 178, 49] },
          { offset: 0.81, color: [247, 178, 49] },
          { offset: 0.815, color: 'transparent' },
          { offset: 0.8725, color: 'transparent' },
          { offset: 0.8775, color: [247, 178, 49] },
          { offset: 0.935, color: [247, 178, 49] },
          { offset: 0.94, color: 'transparent' },
        ]
      },
      foot: [138, 59, 17]
    },
    pig: {
      probability: 0.05,
      main: [244, 172, 186],
      snout: [235, 89, 110],
      holes: [102, 33, 18],
      foot: [171, 120, 130]
    },
    gold: {
      probability: 0.01,
      main: [218, 165, 32],
      sparkle: [252, 228, 159],
      foot: [152, 115, 22]
    },
    frog: {
      probability: 0.05,
      main: [199, 230, 181],
      upper: [120, 180, 84],
      nose: [92, 146, 56],
      foot: [139, 161, 127]
    },
    dog: {
      probability: 0.05,
      furdark: [48, 40, 38],
      furorange: [243, 131, 47],
      furbright: [233, 233, 233],
      mouth: [33, 22, 20],
      tongue: [240, 94, 94],
      ears: [62, 60, 61],
      foot: [140, 140, 140]
    }
  },
  names: [
    'Jimmy',
    'Nelson',
    'Peter',
    'Jeff',
    'Max',
    'Ronald',
    'Ryo',
    'Jenny',
    'Emma',
    'Sakura',
    'Elvira',
    'Julia',
    'Charlotte',
    'Tina',
    'Myne'
  ],
  eventAmount: 3,
  maxSparkles: 10
}

export function init (withColor = true) {
  if (appDyn.wordDifficulty === 1) { // easy, little room for error
    gameDyn.balloons.targetBalloons = Math.max(Math.min(Math.floor(appDyn.wordCount / 3 * 0.9 * Math.random()), 20), 1)
  } else if (appDyn.wordDifficulty === 2) { // medium, some room for error
    gameDyn.balloons.targetBalloons = Math.max(Math.min(Math.floor(appDyn.wordCount / 3 * 0.8 * Math.random()), 20), 1)
  } else { // hard, more room for error
    gameDyn.balloons.targetBalloons = Math.max(Math.min(Math.floor(appDyn.wordCount / 3 * 0.7 * Math.random()), 20), 1)
  }

  data.currentName = Math.floor(gameConst.games.balloons.names.length * Math.random())
  data.currentEvent = Math.floor(gameConst.games.balloons.eventAmount * Math.random()) + 1
  if (withColor) {
    data.currentColor = getBalloonColor()
    if (data.currentColor === 'gold') {
      data.additionalData.sparkleObj = []
    }
  }

  for (let key in constants.colors) {
    if (!Object.hasOwn(data.created, key)) {
      data.created[key] = 0
    }
  }
}

export function canvasClear () {
  gameDyn.basicContext.clearRect(0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight())
}

export function canvasUpdate () {
  if (data.animation.step >= 2) {
    data.animation.counter += 1
    balloonMoveAnim(data.animation.counter, data)

    if (data.animation.counter >= 60) {
      data.currentPumps = 0
      data.balloonPosition = {
        x: 300,
        y: 240
      }
      data.animation.counter = 0
      data.balloonScale = 0.5
      data.currentColor = getBalloonColor()
      if (data.currentColor === 'gold') {
        data.additionalData.sparkleObj = []
      }
      data.animation.step = 1
      if (gameDyn.balloons.targetBalloons === gameDyn.balloons.finishedBalloons) {
        data.switchOrder = true
        data.ordersFinished += 1
        data.animation.counter2 = 0
      }
    }
  } else if (data.currentPumps !== gameDyn.balloons.pumps) {
    if (data.animation.step === 1) {
      data.animation.counter += 1
      balloonPumpAnim(data.animation.counter, data)
      if (data.animation.counter >= 60) {
        data.animation.counter = 0
        data.currentPumps = gameDyn.balloons.pumps
        data.balloonScale = data.currentPumps * 0.5 + 0.5
        if (gameDyn.balloons.pumps >= 3) {
          gameDyn.balloons.finishedBalloons += 1
          data.created[data.currentColor] += 1
          gameDyn.balloons.pumps = 0
          data.animation.step = 2
        }
      }
    }
  }

  if (data.switchOrder) {
    data.animation.counter2 += 1
    clipboardMoveAnim(data.animation.counter2, data)
    if (data.animation.counter2 === 30) {
      init(false)
      gameDyn.balloons.finishedBalloons = 0
    } else if (data.animation.counter2 >= 60) {
      data.switchOrder = false
      data.clipboardPosition.x = 0
    }
  }

  if (Object.hasOwn(data.additionalData, 'sparkleObj')) {
    updateSparkleObj(data.additionalData.sparkleObj)
  }

  for (const shelf of data.finishedBalloonObj) {
    for (const balloonObj of shelf) {
      if (Object.hasOwn(balloonObj, 'additionalData')) {
        if (Object.hasOwn(balloonObj.additionalData, 'sparkleObj')) {
          updateSparkleObj(balloonObj.additionalData.sparkleObj)
        }
      }
    }
  }
}

function updateSparkleObj (sparkleObj) {
  if (sparkleObj.length < gameConst.games.balloons.maxSparkles) {
    while (sparkleObj.length < gameConst.games.balloons.maxSparkles) {
      const xDist = Math.floor(Math.random() * 70) - 35
      const rest = Math.sqrt(1_225 - xDist ** 2)
      const yDist = Math.floor(Math.random() * (rest * 2)) - rest
      sparkleObj.push({
        xDist,
        yDist,
        maxWidth: 10 + Math.floor(Math.random() * 15),
        maxHeight: 10 + Math.floor(Math.random() * 15),
        scale: 0, // from 0 to 1, 1 = max
        speed: 0.003 + Math.random() * 0.02
      })
    }
  }

  for (const sparkle of sparkleObj) {
    sparkle.scale += sparkle.speed
  }
  
  for (let i = sparkleObj.length - 1; i >= 0; i--) {
    if (sparkleObj[i].scale >= 1) {
      sparkleObj.splice(i, 1)
    }
  }
}

export function canvasDraw () {
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight(), [216, 181, 140]
  )
  drawPump()

  drawClipboard(data.clipboardPosition)

  for (let i = 2; i >= 0; i--) {
    drawShelf(94 + i * 85)
    if (data.finishedBalloonObj.length >= i + 1) {
      for (let balloonObj of data.finishedBalloonObj[i]) {
        let pos = {
          x: balloonObj.x + (Math.sin((balloonObj.animationCounter * 0.015)) * 3),
          y: balloonObj.y - (Math.sin((balloonObj.animationCounter * 0.02) - Math.PI / 2) / 2 + 0.5) * 3
        }
        drawABalloon(1, pos, balloonObj.color, true, balloonObj.additionalData || {})
        balloonObj.animationCounter += 1
      }
    }
  }

  drawABalloon(data.balloonScale, data.balloonPosition, data.currentColor, data.animation.step >= 2, data.additionalData)

  BasicHelper.drawCanvasRectBorder(
    gameDyn.basicContext, 5, 5, gameDyn.getCanvasWidth() - 10, gameDyn.getCanvasHeight() - 10, 10, 'miter', [],
    [127, 78, 22]
  )
  BasicHelper.drawCanvasLine(
    gameDyn.basicContext, 12, 12, [0,0,0], 4, 'square', 'miter', [], 1, gameDyn.getCanvasWidth() - 12, 12,
    gameDyn.getCanvasWidth() - 12, gameDyn.getCanvasHeight() - 12
  )
  BasicHelper.drawCanvasLine(
    gameDyn.basicContext, 12, 14, [0,0,0], 4, 'butt', 'miter', [], 1, 12, gameDyn.getCanvasHeight() - 12,
    gameDyn.getCanvasWidth() - 10, gameDyn.getCanvasHeight() - 12
  )
}

function drawClipboard (pos = {x: 0, y: 0}) {
  BasicHelper.drawCanvasRectRound( //board shadow
    gameDyn.basicContext, pos.x + 20, pos.y + 45, 155, 205, 10, [51, 42, 21]
  )
  BasicHelper.drawCanvasRectRound( //board
    gameDyn.basicContext, pos.x + 25, pos.y + 50, 150, 200, 10, [91, 68, 42]
  )
  BasicHelper.drawCanvasRectRoundBorder(
    gameDyn.basicContext, pos.x + 20, pos.y + 45, 155, 205, 10, [0, 0, 0], 3, 'miter', []
  )
  BasicHelper.drawCanvasRectRound( //clip base
    gameDyn.basicContext, pos.x + 65, pos.y + 60, 70, 10, 5, [140, 140, 140]
  )
  BasicHelper.drawCanvasRectRoundBorder( //clip base border
    gameDyn.basicContext, pos.x + 65, pos.y + 60, 70, 10, 5, [0, 0, 0], 3, 'miter', []
  )

  BasicHelper.drawCanvasRect( //paper
    gameDyn.basicContext, pos.x + 40, pos.y + 80, 120, 150, [240, 240, 240]
  )
  BasicHelper.drawCanvasRectBorder(
    gameDyn.basicContext, pos.x + 40, pos.y + 80, 120, 150, 3, 'miter', [], [0, 0, 0]
  )
  BasicHelper.drawCanvasText( //headline
    gameDyn.basicContext, getText('balloonsHeadline'), pos.x + 100, pos.y + 109, 'center', 'middle', 'Nunito',
    16, false, [0, 0, 0]
  )
  BasicHelper.drawCanvasText( //headline
    gameDyn.basicContext, getText(gameConst.games.balloons.names[data.currentName] + "'s"), pos.x + 100, pos.y + 126, 'center', 'middle', 'Nunito', 16,
    false, [0, 0, 0]
  )
  BasicHelper.drawCanvasText( //headline
    gameDyn.basicContext, getText('balloonsEvent' + data.currentEvent), pos.x + 100, pos.y + 143, 'center', 'middle', 'Nunito',
    16, false, [0, 0, 0]
  )
  BasicHelper.drawCanvasText( //target
    gameDyn.basicContext, Math.max(gameDyn.balloons.targetBalloons - gameDyn.balloons.finishedBalloons, 0).toString(),
    pos.x + 100, pos.y + 180, 'center', 'middle', 'Nunito', 40, true, [0, 0, 0]
  )
  BasicHelper.drawCanvasText( //target
    gameDyn.basicContext, getText('balloonsUnit'), pos.x + 100, pos.y + 206, 'center', 'middle', 'Nunito',
    16, false, [0, 0, 0]
  )

  BasicHelper.drawCanvasRect( //clip top
    gameDyn.basicContext, pos.x + 75, pos.y + 65, 50, 5, [110, 110, 110]
  )
  BasicHelper.drawCanvasRect( //clip bottom
    gameDyn.basicContext, pos.x + 75, pos.y + 70, 50, 15, [180, 180, 180]
  )
  BasicHelper.drawCanvasRectBorder(
    gameDyn.basicContext, pos.x + 75, pos.y + 65, 50, 20, 3, 'miter', [], [0, 0, 0]
  )
}

function drawABalloon (scale = 1, balloonPos = { x: 300, y: 240 }, color = 'purple', scaleBottom = false, additionalData = {}) {
  switch (color) {
    case 'redpanda':
      drawRedPanda(scale, balloonPos, scaleBottom)
      break
    case 'pig':
      drawPig(scale, balloonPos, scaleBottom)
      break
    case 'frog':
      drawFrog(scale, balloonPos, scaleBottom)
      break
    case 'dog':
      drawDog(scale, balloonPos, scaleBottom)
      break
    case 'applepie':
      drawApplePie(scale, balloonPos, color, scaleBottom)
      break
    default:
      drawBalloon(scale, balloonPos, color, scaleBottom, additionalData)
  }
}

function drawBalloon (scale = 1, balloonPos = { x: 300, y: 240 }, color = 'purple', scaleBottom = false, additionalData = {}) {
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    gameConst.games.balloons.colors[color].main
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 10 * scale, balloonPos.y - 45 * scale, 6 * scale, 3 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 160
  )

  const outerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0
  )
  const innerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x + 3.5 * scale, balloonPos.y - 38.5 * scale, 25 * scale, 18 * scale, 4.2 * scale, 0
  )
  outerPath.addPath(innerPath)
  gameDyn.basicContext.save()
  gameDyn.basicContext.clip(outerPath, 'evenodd')
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight(), [0, 0, 0], 0.4
  )
  gameDyn.basicContext.restore()

  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    [0, 0, 0], 3, []
  )

  let bottomSize = -12
  if (scaleBottom) {
    bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
    gameConst.games.balloons.colors[color].foot
  )
  BasicHelper.drawCanvasHeartBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )

  if (color === 'gold') {
    for (const sparkle of additionalData.sparkleObj) {
      const width = sparkle.maxWidth * scale * sparkle.scale
      const height = sparkle.maxHeight * scale * sparkle.scale
      BasicHelper.drawCanvasSparkle(
        gameDyn.basicContext, balloonPos.x + sparkle.xDist * scale - width / 2,
        balloonPos.y + (sparkle.yDist - 30) * scale - height / 2, width, height,
        width * 0.5, height * 0.5, gameConst.games.balloons.colors.gold.sparkle,
        Math.max(Math.min(5 - (5 * sparkle.scale), 1), 0)
      )
    }
  }
}

function drawApplePie (scale = 1, balloonPos = { x: 300, y: 240 }, color = 'applepie', scaleBottom = false) {
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    gameConst.games.balloons.colors[color].background
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    gameConst.games.balloons.colors[color].main
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    gameConst.games.balloons.colors[color].mainneg
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 10 * scale, balloonPos.y - 45 * scale, 6 * scale, 3 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 160
  )

  const outerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0
  )
  const innerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x + 3.5 * scale, balloonPos.y - 38.5 * scale, 25 * scale, 18 * scale, 4.2 * scale, 0
  )
  outerPath.addPath(innerPath)
  gameDyn.basicContext.save()
  gameDyn.basicContext.clip(outerPath, 'evenodd')
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight(), [0, 0, 0], 0.4
  )
  gameDyn.basicContext.restore()

  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 35 * scale, 30 * scale, 23 * scale, 5 * scale, 0,
    [0, 0, 0], 3, []
  )

  let bottomSize = -12
  if (scaleBottom) {
    bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
    gameConst.games.balloons.colors[color].foot
  )
  BasicHelper.drawCanvasHeartBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )
}

function drawRedPanda (scale = 1, balloonPos = { x: 300, y: 240 }, scaleBottom = false) {
  const colors = gameConst.games.balloons.colors.redpanda
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 17 * scale, balloonPos.y - 37 * scale, 17 * scale, 14.5 * scale, 17 * scale, 0,
    colors.white, 1, 145
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 23 * scale, balloonPos.y - 45 * scale, 8 * scale, 6 * scale, 8 * scale, 0,
    [0, 0, 0], 0.35, 145
  )
  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x - 17 * scale, balloonPos.y - 37 * scale, 17 * scale, 14.5 * scale, 17 * scale, 0,
    [0, 0, 0], 3, [], 1, 145
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 17 * scale, balloonPos.y - 37 * scale, 17 * scale, 14.5 * scale, -17 * scale, 0,
    colors.white, 1, 35
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 23 * scale, balloonPos.y - 45 * scale, 8 * scale, 6 * scale, -8 * scale, 0,
    [0, 0, 0], 0.35, 35
  )
  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x + 17 * scale, balloonPos.y - 37 * scale, 17 * scale, 14.5 * scale, -17 * scale, 0,
    [0, 0, 0], 3, [], 1, 35
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 24 * scale, 30 * scale, 35 * scale, -6 * scale, 0,
    colors.main
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 18 * scale, 18 * scale, 28 * scale,
    0, 0, colors.white
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 17 * scale, 19 * scale, 21 * scale,
    -2 * scale, 0, colors.snout
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 29 * scale, 10 * scale, 15 * scale,
    0, 0, colors.main
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 15 * scale, 12 * scale, 14 * scale,
    0, 0, colors.white
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 15 * scale, 4 * scale, 5 * scale,
    0, 0, [0, 0, 0]
  )
  BasicHelper.drawCanvasRectRound(
    gameDyn.basicContext, balloonPos.x - 8 * scale, balloonPos.y - 41 * scale, 5 * scale, 5 * scale, 2.5 * scale,
    colors.white, 1, true, true, true, false, 20
  )
  BasicHelper.drawCanvasRectRound(
    gameDyn.basicContext, balloonPos.x + 8 * scale, balloonPos.y - 41 * scale, 5 * scale, 5 * scale, 2.5 * scale,
    colors.white, 1, true, true, true, false, 70
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 10 * scale, balloonPos.y - 29 * scale, 4 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 10 * scale, balloonPos.y - 29 * scale, 4 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 17 * scale, balloonPos.y - 46 * scale, 7 * scale, 4 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 140
  )

  const outerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x, balloonPos.y - 24 * scale, 30 * scale, 35 * scale, -6 * scale, 0
  )
  const innerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x + 4 * scale, balloonPos.y - 24 * scale, 27 * scale, 30 * scale, -8 * scale, 0
  )
  outerPath.addPath(innerPath)
  gameDyn.basicContext.save()
  gameDyn.basicContext.clip(outerPath, 'evenodd')
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight(), [0, 0, 0], 0.4
  )
  gameDyn.basicContext.restore()

  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 24 * scale, 30 * scale, 35 * scale, -6 * scale, 0,
    [0, 0, 0], 3, []
  )

  let bottomSize = -12
  if (scaleBottom) {
    bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
    colors.foot
  )
  BasicHelper.drawCanvasHeartBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )
}

function drawPig (scale = 1, balloonPos = { x: 200, y: 250 }, scaleBottom = false) {
  const colors = gameConst.games.balloons.colors.pig
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 21 * scale, balloonPos.y - 43 * scale, 12 * scale, 12 * scale,
    6 * scale, 0, colors.main, 1, 145, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 18 * scale, balloonPos.y - 37 * scale, 12 * scale, 10 * scale,
    6 * scale, 0, colors.snout, 1, 145, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x - 21 * scale, balloonPos.y - 43 * scale, 12 * scale, 12 * scale,
    6 * scale, 0, [0, 0, 0], 3, [], 1, 145, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 21 * scale, balloonPos.y - 43 * scale, 12 * scale, 12 * scale,
    6 * scale, 0, colors.main, 1, 215, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 18 * scale, balloonPos.y - 37 * scale, 12 * scale, 10 * scale,
    6 * scale, 0, colors.snout, 1, 215, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x + 21 * scale, balloonPos.y - 43 * scale, 12 * scale, 12 * scale,
    6 * scale, 0, [0, 0, 0], 3, [], 1, 215, [0.3, 0.6, 0.6, 0.3]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 19 * scale, 28 * scale, 32 * scale, -9 * scale, 0,
    colors.main, 1, 0, [0.65, 0.5522848, 0.5522848, 0.65]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 17 * scale, balloonPos.y - 29 * scale, 4 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 17 * scale, balloonPos.y - 29 * scale, 4 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 19 * scale, 11 * scale, 15 * scale, 3 * scale, 0, colors.snout,
    1, 0, [0.5522848, 0.8, 0.8, 0.5522848]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 5 * scale, balloonPos.y - 17 * scale, 4 * scale, 2.5 * scale,
    0, 0, colors.holes, 1, 0, [0.8, 0.8, 0.8, 0.8]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 5 * scale, balloonPos.y - 17 * scale, 4 * scale, 2.5 * scale,
    0, 0, colors.holes, 1, 0, [0.8, 0.8, 0.8, 0.8]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 12 * scale, balloonPos.y - 44 * scale, 6 * scale, 3 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 140
  )

  const outerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x, balloonPos.y - 19 * scale, 28 * scale, 32 * scale, -9 * scale, 0, [0.65, 0.5522848, 0.5522848, 0.65]
  )
  const innerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x + 4 * scale, balloonPos.y - 19 * scale, 25 * scale, 27 * scale, -11 * scale, 0, [0.65, 0.5522848, 0.5522848, 0.65]
  )
  outerPath.addPath(innerPath)
  gameDyn.basicContext.save()
  gameDyn.basicContext.clip(outerPath, 'evenodd')
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, gameDyn.getCanvasWidth(), gameDyn.getCanvasHeight(), [0, 0, 0], 0.4
  )
  gameDyn.basicContext.restore()
  BasicHelper.drawCanvasEllipseCustomBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 19 * scale, 28 * scale, 32 * scale, -9 * scale, 0, [0, 0, 0], 3, [],
    1, 0, [0.65, 0.5522848, 0.5522848, 0.65]
  )
  
  let bottomSize = -12
  if (scaleBottom) {
    bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
    colors.foot
  )
  BasicHelper.drawCanvasHeartBorder(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )
}

function drawFrog (scale = 1, balloonPos = { x: 200, y: 250 }, scaleBottom = false) {
  const colors = gameConst.games.balloons.colors.frog
  BasicHelper.drawCanvasEllipseCustom(
      gameDyn.basicContext, balloonPos.x, balloonPos.y - 24 * scale, 28 * scale, 36 * scale, -4 * scale, 0,
      colors.main, 1, 0, [0.5522848, 0.45, 0.45, 0.5522848]
  )
  BasicHelper.drawCanvasEllipseCustom(
      gameDyn.basicContext, balloonPos.x, balloonPos.y - 24 * scale, 16 * scale, 36 * scale, -16 * scale, 0,
      colors.upper, 1, 0, [0.5522848, 0.45, 0.45, 0.5522848]
  )
  BasicHelper.drawCanvasEllipseCustom(
      gameDyn.basicContext, balloonPos.x - 30 * scale, balloonPos.y - 24 * scale, 4 * scale, 12 * scale, 2 * scale, 6 * scale,
      colors.upper, 1, 0, [0.6, 0.5522848, 0.5522848, 0.4]
  )
  BasicHelper.drawCanvasEllipseCustom(
      gameDyn.basicContext, balloonPos.x + 30 * scale, balloonPos.y - 24 * scale, 4 * scale, 12 * scale, 2 * scale, -6 * scale,
      colors.upper, 1, 0, [0.4, 0.5522848, 0.5522848, 0.6]
  )
  BasicHelper.drawCanvasEllipseCustom(
      gameDyn.basicContext, balloonPos.x, balloonPos.y - 20 * scale, 4 * scale, 18 * scale, -6 * scale, 0,
      colors.upper, 1, 0, [0.4, 0.5522848, 0.5522848, 0.4]
  )
  BasicHelper.drawCanvasEllipseCustomBorder(
      gameDyn.basicContext, balloonPos.x, balloonPos.y - 24 * scale, 28 * scale, 36 * scale, -4 * scale, 0,
      [0, 0, 0], 3, [], 1, 0, [0.5522848, 0.45, 0.45, 0.5522848]
  )
  BasicHelper.drawCanvasCircle( // nose
    gameDyn.basicContext, balloonPos.x - 8 * scale, balloonPos.y - 28 * scale, 2 * scale, colors.nose
  )
  BasicHelper.drawCanvasCircle( // nose
    gameDyn.basicContext, balloonPos.x + 8 * scale, balloonPos.y - 28 * scale, 2 * scale, colors.nose
  )
  BasicHelper.drawCanvasCircle( // eye left
    gameDyn.basicContext, balloonPos.x - 21 * scale, balloonPos.y - 50 * scale, 11 * scale, colors.upper
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 21.5 * scale, balloonPos.y - 51 * scale, 6 * scale, [255, 255, 255]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 21.5 * scale, balloonPos.y - 51 * scale, 2.5 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasCircleBorder(
    gameDyn.basicContext, balloonPos.x - 21 * scale, balloonPos.y - 50 * scale, 11 * scale, 3, [], [0, 0, 0], 1,
    Math.PI * 0.7, Math.PI * 1.92
  )

  const outerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x, balloonPos.y - 24 * scale, 28 * scale, 36 * scale, -4 * scale, 0, [0.5522848, 0.45, 0.45, 0.5522848]
  )
  const innerPath = BasicHelper.getCanvasEllipseCustomPath(
    balloonPos.x + 4 * scale, balloonPos.y - 24 * scale, 25 * scale, 31 * scale, -6 * scale, 0, [0.5522848, 0.45, 0.45, 0.5522848], true
  )
  const eyePath = new Path2D()
  eyePath.arc(balloonPos.x - 21 * scale, balloonPos.y - 50 * scale, 11 * scale, Math.PI * 0.7, Math.PI * 1.92)
  eyePath.addPath(outerPath)
  eyePath.addPath(innerPath)

  gameDyn.basicContext.save()
  gameDyn.basicContext.clip(eyePath)
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 0, 0, canvas.width, canvas.height, [0, 0, 0], 0.4
  )
  gameDyn.basicContext.restore()
  
  BasicHelper.drawCanvasCircle( // eye right
    gameDyn.basicContext, balloonPos.x + 21 * scale, balloonPos.y - 50 * scale, 11 * scale, colors.upper
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 21.5 * scale, balloonPos.y - 51 * scale, 6 * scale, [255, 255, 255]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 21.5 * scale, balloonPos.y - 51 * scale, 2.5 * scale, [0, 0, 0]
  )
  BasicHelper.drawCanvasCircleBorder(
    gameDyn.basicContext, balloonPos.x + 21 * scale, balloonPos.y - 50 * scale, 11 * scale, 3, [], [0, 0, 0], 1,
    Math.PI * 1.08, Math.PI * 2.3
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 16 * scale, balloonPos.y - 40 * scale, 6 * scale, 3 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 140
  )

  let bottomSize = -12
  if (scaleBottom) {
      bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
      gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
      colors.foot
  )
  BasicHelper.drawCanvasHeartBorder(
      gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )
}

function drawDog (scale = 1, balloonPos = { x: 200, y: 250 }, scaleBottom = false) {
  scale *= 0.7
  const colors = gameConst.games.balloons.colors.dog
  let orangeY = 53
  let mouthY = 41
  let noseY = 84
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, colors.furbright, 1,
    [
      balloonPos.x - 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x - 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x - 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x - 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x - 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x - 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x - 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x - 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x - 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x + 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      
      balloonPos.x + 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x + 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x + 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x + 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x + 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x + 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x + 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x + 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x, balloonPos.y
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 87 * scale, colors.furdark, 1,
    [
      balloonPos.x + 40 * scale, balloonPos.y - 87 * scale,
      balloonPos.x + 31 * scale, balloonPos.y - 55 * scale,
      balloonPos.x + 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x - 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x - 31 * scale, balloonPos.y - 55 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 87 * scale,
      balloonPos.x, balloonPos.y - 87 * scale
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - orangeY * scale, colors.furorange, 1,
    [
      balloonPos.x + 17 * scale, balloonPos.y - orangeY * scale,
      balloonPos.x + 19 * scale, balloonPos.y - (orangeY + 5) * scale,
      balloonPos.x + 23 * scale, balloonPos.y - (orangeY + 5) * scale
    ],
    [
      balloonPos.x + 26 * scale, balloonPos.y - (orangeY + 5) * scale,
      balloonPos.x + 28 * scale, balloonPos.y - (orangeY - 11) * scale,
      balloonPos.x + 28 * scale, balloonPos.y - (orangeY - 13) * scale
    ],
    [
      balloonPos.x + 28 * scale, balloonPos.y - (orangeY - 15) * scale,
      balloonPos.x + 25.5 * scale, balloonPos.y - (orangeY - 20) * scale,
      balloonPos.x + 25 * scale, balloonPos.y - (orangeY - 20) * scale
    ],
    [
      balloonPos.x + 19 * scale, balloonPos.y - (orangeY - 19) * scale,
      balloonPos.x + 23 * scale, balloonPos.y - (orangeY - 13) * scale,
      balloonPos.x, balloonPos.y - (orangeY - 13) * scale
    ],
    [
      balloonPos.x - 23 * scale, balloonPos.y - (orangeY - 13) * scale,
      balloonPos.x - 19 * scale, balloonPos.y - (orangeY - 19) * scale,
      balloonPos.x - 25 * scale, balloonPos.y - (orangeY - 20) * scale
    ],
    [
      balloonPos.x - 25.5 * scale, balloonPos.y - (orangeY - 20) * scale,
      balloonPos.x - 28 * scale, balloonPos.y - (orangeY - 15) * scale,
      balloonPos.x - 28 * scale, balloonPos.y - (orangeY - 13) * scale
    ],
    [
      balloonPos.x - 28 * scale, balloonPos.y - (orangeY - 11) * scale,
      balloonPos.x - 26 * scale, balloonPos.y - (orangeY + 5) * scale,
      balloonPos.x - 23 * scale, balloonPos.y - (orangeY + 5) * scale
    ],
    [
      balloonPos.x - 19 * scale, balloonPos.y - (orangeY + 5) * scale,
      balloonPos.x - 17 * scale, balloonPos.y - orangeY * scale,
      balloonPos.x, balloonPos.y - orangeY * scale
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - mouthY * scale, colors.mouth, 1,
    [
      balloonPos.x + 5 * scale, balloonPos.y - mouthY * scale,
      balloonPos.x + 12 * scale, balloonPos.y - (mouthY + 4) * scale,
      balloonPos.x + 16 * scale, balloonPos.y - (mouthY + 6) * scale
    ],
    [
      balloonPos.x + 13 * scale, balloonPos.y - mouthY * scale,
      balloonPos.x + 10 * scale, balloonPos.y - (mouthY - 11) * scale,
      balloonPos.x, balloonPos.y - (mouthY - 11) * scale
    ],
    [
      balloonPos.x - 10 * scale, balloonPos.y - (mouthY - 11) * scale,
      balloonPos.x - 13 * scale, balloonPos.y - mouthY * scale,
      balloonPos.x - 16 * scale, balloonPos.y - (mouthY + 6) * scale
    ],
    [
      balloonPos.x - 12 * scale, balloonPos.y - (mouthY + 4) * scale,
      balloonPos.x - 5 * scale, balloonPos.y - mouthY * scale,
      balloonPos.x, balloonPos.y - mouthY * scale
    ]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 40 * scale, 7 * scale, 8 * scale, 7 * scale, 0,
    colors.tongue, 1, 0, [0.8, 0.7, 0.7, 0.8]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - noseY * scale, colors.furbright, 1,
    [
      balloonPos.x + 2 * scale, balloonPos.y - noseY * scale,
      balloonPos.x + 3.75 * scale, balloonPos.y - (noseY - 4) * scale,
      balloonPos.x + 3.75 * scale, balloonPos.y - (noseY - 7) * scale
    ],
    [
      balloonPos.x + 3.75 * scale, balloonPos.y - (noseY - 10) * scale,
      balloonPos.x + 3 * scale, balloonPos.y - (noseY - 13) * scale,
      balloonPos.x + 3 * scale, balloonPos.y - (noseY - 16) * scale
    ],
    [
      balloonPos.x + 3 * scale, balloonPos.y - (noseY - 24) * scale,
      balloonPos.x + 12 * scale, balloonPos.y - (noseY - 32) * scale,
      balloonPos.x + 12 * scale, balloonPos.y - (noseY - 36) * scale
    ],
    [
      balloonPos.x + 12 * scale, balloonPos.y - (noseY - 43) * scale,
      balloonPos.x + 8 * scale, balloonPos.y - (noseY - 45.5) * scale,
      balloonPos.x, balloonPos.y - (noseY - 44) * scale
    ],
    [
      balloonPos.x - 8 * scale, balloonPos.y - (noseY - 45.5) * scale,
      balloonPos.x - 12 * scale, balloonPos.y - (noseY - 43) * scale,
      balloonPos.x - 12 * scale, balloonPos.y - (noseY - 36) * scale
    ],
    [
      balloonPos.x - 12 * scale, balloonPos.y - (noseY - 32) * scale,
      balloonPos.x - 3 * scale, balloonPos.y - (noseY - 24) * scale,
      balloonPos.x - 3 * scale, balloonPos.y - (noseY - 16) * scale
    ],
    [
      balloonPos.x - 3 * scale, balloonPos.y - (noseY - 13) * scale,
      balloonPos.x - 3.75 * scale, balloonPos.y - (noseY - 10) * scale,
      balloonPos.x - 3.75 * scale, balloonPos.y - (noseY - 7) * scale
    ],
    [
      balloonPos.x - 3.75 * scale, balloonPos.y - (noseY - 4) * scale,
      balloonPos.x - 2 * scale, balloonPos.y - noseY * scale,
      balloonPos.x, balloonPos.y - noseY * scale
    ]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x, balloonPos.y - 48 * scale, 5 * scale, 7 * scale, 1 * scale, 0,
    colors.furdark, 1, 0, [0.5522848, 0.7, 0.7, 0.5522848]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 12 * scale, balloonPos.y - 65 * scale, 4 * scale, colors.furbright
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x - 12 * scale, balloonPos.y - 65 * scale, 2.5 * scale, colors.furdark
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x - 11 * scale, balloonPos.y - 72 * scale, 2 * scale, 3 * scale, -1 * scale, 0,
    colors.furorange, 1, 20, [0.5522848, 0.5522848, 0.5522848, 0.5522848]
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 12 * scale, balloonPos.y - 65 * scale, 4 * scale, colors.furbright
  )
  BasicHelper.drawCanvasCircle(
    gameDyn.basicContext, balloonPos.x + 12 * scale, balloonPos.y - 65 * scale, 2.5 * scale, colors.furdark
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 11 * scale, balloonPos.y - 72 * scale, 2 * scale, 3 * scale, 1 * scale, 0,
    colors.furorange, 1, 160, [0.5522848, 0.5522848, 0.5522848, 0.5522848]
  )
  BasicHelper.drawCanvasBezierLine(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, [0, 0, 0], 3, 'round', 'miter', [], 1,
    [
      balloonPos.x - 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x - 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x - 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x - 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x - 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x - 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x - 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x - 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x - 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x - 31 * scale, balloonPos.y - 55 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 87 * scale,
      balloonPos.x, balloonPos.y - 87 * scale
    ],
    [
      balloonPos.x + 40 * scale, balloonPos.y - 87 * scale,
      balloonPos.x + 31 * scale, balloonPos.y - 55 * scale,
      balloonPos.x + 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x + 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x + 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x + 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x + 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x + 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x + 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x + 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x + 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x, balloonPos.y
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x, balloonPos.y, [0, 0, 0], 0.4,
    [
      balloonPos.x - 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x - 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x - 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x - 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x - 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x - 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x - 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x - 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x - 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x - 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x - 31 * scale, balloonPos.y - 55 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 87 * scale,
      balloonPos.x + 5 * scale, balloonPos.y - 87 * scale
    ],
    [
      balloonPos.x - 60 * scale, balloonPos.y - 57 * scale,
      balloonPos.x + 0 * scale, balloonPos.y + 38 * scale,
      balloonPos.x + 29 * scale, balloonPos.y - 40 * scale
    ],
    [
      balloonPos.x + 31 * scale, balloonPos.y - 25 * scale,
      balloonPos.x + 27 * scale, balloonPos.y - 20 * scale,
      balloonPos.x + 24 * scale, balloonPos.y - 14 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 20 * scale
    ],
    [
      balloonPos.x + 22 * scale, balloonPos.y - 16 * scale,
      balloonPos.x + 16 * scale, balloonPos.y - 8 * scale,
      balloonPos.x + 12 * scale, balloonPos.y - 6 * scale
    ],
    [
      balloonPos.x + 12 * scale, balloonPos.y - 11 * scale
    ],
    [
      balloonPos.x + 9 * scale, balloonPos.y - 4 * scale,
      balloonPos.x + 2 * scale, balloonPos.y - 1 * scale,
      balloonPos.x, balloonPos.y
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x + 10 * scale, balloonPos.y - 86 * scale, colors.ears, 1,
    [
      balloonPos.x + 12 * scale, balloonPos.y - 86 * scale,
      balloonPos.x + 18 * scale, balloonPos.y - 88 * scale,
      balloonPos.x + 20 * scale, balloonPos.y - 88 * scale
    ],
    [
      balloonPos.x + 25 * scale, balloonPos.y - 87 * scale,
      balloonPos.x + 43 * scale, balloonPos.y - 80 * scale,
      balloonPos.x + 45 * scale, balloonPos.y - 75 * scale
    ],
    [
      balloonPos.x + 45 * scale, balloonPos.y - 67 * scale,
      balloonPos.x + 42 * scale, balloonPos.y - 65 * scale,
      balloonPos.x + 40 * scale, balloonPos.y - 55 * scale
    ],
    [
      balloonPos.x + 40 * scale, balloonPos.y - 53 * scale,
      balloonPos.x + 37 * scale, balloonPos.y - 50 * scale,
      balloonPos.x + 35 * scale, balloonPos.y - 50 * scale
    ],
    [
      balloonPos.x + 26 * scale, balloonPos.y - 53 * scale,
      balloonPos.x + 25 * scale, balloonPos.y - 86 * scale,
      balloonPos.x + 10 * scale, balloonPos.y - 86 * scale
    ]
  )
  BasicHelper.drawCanvasBezierLine(
    gameDyn.basicContext, balloonPos.x + 10 * scale, balloonPos.y - 86 * scale, [0, 0, 0], 3, 'round', 'miter', [], 1,
    [
      balloonPos.x + 12 * scale, balloonPos.y - 86 * scale,
      balloonPos.x + 18 * scale, balloonPos.y - 88 * scale,
      balloonPos.x + 20 * scale, balloonPos.y - 88 * scale
    ],
    [
      balloonPos.x + 25 * scale, balloonPos.y - 87 * scale,
      balloonPos.x + 43 * scale, balloonPos.y - 80 * scale,
      balloonPos.x + 45 * scale, balloonPos.y - 75 * scale
    ],
    [
      balloonPos.x + 45 * scale, balloonPos.y - 67 * scale,
      balloonPos.x + 42 * scale, balloonPos.y - 65 * scale,
      balloonPos.x + 40 * scale, balloonPos.y - 55 * scale
    ],
    [
      balloonPos.x + 40 * scale, balloonPos.y - 53 * scale,
      balloonPos.x + 37 * scale, balloonPos.y - 50 * scale,
      balloonPos.x + 35 * scale, balloonPos.y - 50 * scale
    ],
    [
      balloonPos.x + 26 * scale, balloonPos.y - 53 * scale,
      balloonPos.x + 25 * scale, balloonPos.y - 86 * scale,
      balloonPos.x + 10 * scale, balloonPos.y - 86 * scale
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x - 10 * scale, balloonPos.y - 86 * scale, colors.ears, 1,
    [
      balloonPos.x - 12 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 18 * scale, balloonPos.y - 88 * scale,
      balloonPos.x - 20 * scale, balloonPos.y - 88 * scale
    ],
    [
      balloonPos.x - 25 * scale, balloonPos.y - 87 * scale,
      balloonPos.x - 43 * scale, balloonPos.y - 80 * scale,
      balloonPos.x - 45 * scale, balloonPos.y - 75 * scale
    ],
    [
      balloonPos.x - 45 * scale, balloonPos.y - 67 * scale,
      balloonPos.x - 42 * scale, balloonPos.y - 65 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 55 * scale
    ],
    [
      balloonPos.x - 40 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 37 * scale, balloonPos.y - 50 * scale,
      balloonPos.x - 35 * scale, balloonPos.y - 50 * scale
    ],
    [
      balloonPos.x - 26 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 25 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 10 * scale, balloonPos.y - 86 * scale
    ]
  )
  BasicHelper.drawCanvasBezierLine(
    gameDyn.basicContext, balloonPos.x - 10 * scale, balloonPos.y - 86 * scale, [0, 0, 0], 3, 'round', 'miter', [], 1,
    [
      balloonPos.x - 12 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 18 * scale, balloonPos.y - 88 * scale,
      balloonPos.x - 20 * scale, balloonPos.y - 88 * scale
    ],
    [
      balloonPos.x - 25 * scale, balloonPos.y - 87 * scale,
      balloonPos.x - 43 * scale, balloonPos.y - 80 * scale,
      balloonPos.x - 45 * scale, balloonPos.y - 75 * scale
    ],
    [
      balloonPos.x - 45 * scale, balloonPos.y - 67 * scale,
      balloonPos.x - 42 * scale, balloonPos.y - 65 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 55 * scale
    ],
    [
      balloonPos.x - 40 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 37 * scale, balloonPos.y - 50 * scale,
      balloonPos.x - 35 * scale, balloonPos.y - 50 * scale
    ],
    [
      balloonPos.x - 26 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 25 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 10 * scale, balloonPos.y - 86 * scale
    ]
  )
  BasicHelper.drawCanvasBezierPolygon(
    gameDyn.basicContext, balloonPos.x - 10 * scale, balloonPos.y - 86 * scale, [0, 0, 0], 0.4,
    [
      balloonPos.x - 12 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 18 * scale, balloonPos.y - 88 * scale,
      balloonPos.x - 20 * scale, balloonPos.y - 88 * scale
    ],
    [
      balloonPos.x - 25 * scale, balloonPos.y - 87 * scale,
      balloonPos.x - 43 * scale, balloonPos.y - 80 * scale,
      balloonPos.x - 45 * scale, balloonPos.y - 75 * scale
    ],
    [
      balloonPos.x - 45 * scale, balloonPos.y - 67 * scale,
      balloonPos.x - 42 * scale, balloonPos.y - 65 * scale,
      balloonPos.x - 40 * scale, balloonPos.y - 55 * scale
    ],
    [
      balloonPos.x - 40 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 37 * scale, balloonPos.y - 50 * scale,
      balloonPos.x - 35 * scale, balloonPos.y - 50 * scale
    ],
    [
      balloonPos.x - 26 * scale, balloonPos.y - 53 * scale,
      balloonPos.x - 25 * scale, balloonPos.y - 86 * scale,
      balloonPos.x - 10 * scale, balloonPos.y - 86 * scale
    ]
  )
  BasicHelper.drawCanvasEllipseCustom(
    gameDyn.basicContext, balloonPos.x + 17 * scale, balloonPos.y - 71 * scale, 6 * scale, 3 * scale, 1 * scale,
    0, [255, 255, 255], 0.7, 155
  )
  let bottomSize = -12
  if (scaleBottom) {
      bottomSize *= scale / 2
  }
  BasicHelper.drawCanvasHeart(
      gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize,
      colors.foot
  )
  BasicHelper.drawCanvasHeartBorder(
      gameDyn.basicContext, balloonPos.x, balloonPos.y, bottomSize, [0, 0, 0], 3, 'butt', []
  )
}

function drawPump () {
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 290, 260, 7, 40, [173, 78, 10]
  )
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 297, 260, 13, 40, [234, 105, 14]
  )
  BasicHelper.drawCanvasRectBorder(
    gameDyn.basicContext, 290, 260, 20, 40, 3, 'miter', [], [0, 0, 0]
  )
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 297, 245, 6, 15, [172, 173, 175]
  )
  BasicHelper.drawCanvasRectBorder(
    gameDyn.basicContext, 297, 245, 6, 15, 3, 'miter', [], [0, 0, 0]
  )
}

function drawShelf (y) {
  BasicHelper.drawCanvasPolygon(
    gameDyn.basicContext, 420, y, [160, 110, 77], 1, 600, y, 600, y + 8, 427, y + 8
  )
  BasicHelper.drawCanvasRect(
    gameDyn.basicContext, 427, y + 8, 173, 8, [112, 68, 39]
  )
  BasicHelper.drawCanvasPolygon(
    gameDyn.basicContext, 420, y, [43, 27, 16], 1, 427, y + 8, 427, y + 16, 420, y + 8
  )
  BasicHelper.drawCanvasLine(
    gameDyn.basicContext, 600, y, [0, 0, 0], 3, 'butt', 'miter', [], 1, 420, y, 420, y + 8, 427, y + 16, 600, y + 16
  )
}

function balloonPumpAnim (animCounter, localData) {
  if (animCounter <= 5) {
    localData.balloonScale -= 0.0025
  } else if (animCounter <= 10) {
    localData.balloonScale += 0.005
  } else if (animCounter <= 50) {
    localData.balloonScale += 0.011875
  } else if (animCounter <= 55) {
    localData.balloonScale += 0.005
  } else {
    localData.balloonScale -= 0.0025
  }
}

function balloonMoveAnim (animCounter, localData) {
  let balloonIndex = localData.finishedBalloonObj.flat().length % 3
  if (localData.finishedBalloonObj.length <= balloonIndex) {
    localData.finishedBalloonObj.push([])
  }
  for (let balloonObj of localData.finishedBalloonObj[balloonIndex]) {
    balloonObj.x += 0.3
  }

  if (balloonIndex === 0) {
    localData.balloonPosition.x += (450 - 300) / 60
    localData.balloonPosition.y += (90 - 240) / 60
  } else if (balloonIndex === 1) {
    localData.balloonPosition.x += (450 - 300) / 60
    localData.balloonPosition.y += (175 - 240) / 60
  } else {
    localData.balloonPosition.x += (450 - 300) / 60
    localData.balloonPosition.y += (260 - 240) / 60
  }
  localData.balloonScale -= 1 / 60

  if (animCounter === 60) {
    const newObj = {
      x: Math.round(localData.balloonPosition.x),
      y: Math.round(localData.balloonPosition.y),
      animationCounter: 0,
      color: data.currentColor
    }
    if (Object.hasOwn(data.additionalData, 'sparkleObj')) {
      newObj.additionalData = {
        sparkleObj: data.additionalData.sparkleObj
      }
      delete data.additionalData.sparkleObj
    }
    localData.finishedBalloonObj[balloonIndex].push(newObj)
  }
}

function clipboardMoveAnim (animCounter, localData) {
  if (animCounter <= 5) {
    localData.clipboardPosition.x -= 1
  } else if (animCounter <= 10) {
    localData.clipboardPosition.x -= 5
  } else if (animCounter <= 30) {
    localData.clipboardPosition.x -= 10
  } else if (animCounter <= 50) {
    localData.clipboardPosition.x += 10
  } else if (animCounter <= 55) {
    localData.clipboardPosition.x += 5
  } else if (animCounter <= 60) {
    localData.clipboardPosition.x += 1
  }
}

function getBalloonColor () {
  const colors = Object.keys(gameConst.games.balloons.colors)
  let prob = 0
  for (let color of colors) {
    prob += gameConst.games.balloons.colors[color].probability
  }
  prob *= Math.random()
  for (let color of colors) {
    prob -= gameConst.games.balloons.colors[color].probability
    if (prob <= 0) {
      return color
    }
  }
  return 'purple'
}

export function getSaveData () {
  return {
    created: data.created,
    ordersFinished: data.ordersFinished
  }
}

export function reset () {
  data.clipboardPosition = {
    x: 0,
    y: 0
  }
  data.balloonPosition = {
    x: 300,
    y: 240
  }
  data.balloonScale = 0.5
  data.currentPumps = 0
  data.finishedBalloonObj = []
  data.animation = {
    counter: 0,
    counter2: 0,
    step: 1
  }
  for (let key in data.created) {
    data.created[key] = 0
  }
  data.ordersFinished = 0
  gameDyn.balloons.pumps = 0
  gameDyn.balloons.finishedBalloons = 0
}

function getText(id, ...params) {
  return appConst.getText(id, ...params)
}