/**
 * Draws a filled rectangle onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} x x-coordinate of the rectangle's top-left corner
 * @param {number} y y-coordinate of the rectangle's top-left corner
 * @param {number} width width of the rectangle
 * @param {number} height height of the rectangle
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the rect
 */
export function drawCanvasRect (context, x, y, width, height, color, opacity = 1) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.fillRect(x, y, width, height)
}

/**
 * Draws a border to a rectangle onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} x x-coordinate of the rectangle's top-left corner
 * @param {number} y y-coordinate of the rectangle's top-left corner
 * @param {number} width width of the rectangle
 * @param {number} height height of the rectangle
 * @param {number} thickness the width of the border
 * @param {string} lineJoin the outer corner style of the line ('round', 'bevel', 'miter')
 * @param {Array} lineDash the dashstyle of the border in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the border
 */
export function drawCanvasRectBorder (context, x, y, width, height, thickness, lineJoin, lineDash, color, opacity = 1) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.lineJoin = lineJoin
  context.setLineDash(lineDash)
  context.strokeRect(x, y, width, height)
}

/**
 * Draws a circle onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} centerX x-center of the circle
 * @param {number} centerY y-center of the circle
 * @param {number} radius radius of the circle
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the circle
 * @param {number} startAngle the start angle of the circle in radians
 * @param {number} endAngle the end angle of the circle in radians
 */
export function drawCanvasCircle (context, centerX, centerY, radius, color, opacity = 1, startAngle = 0, endAngle = Math.PI * 2) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.beginPath()
  context.arc(centerX, centerY, radius, startAngle, endAngle)
  context.fill()
}

/**
 * Draws a border of a circle onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} centerX x-center of the circle
 * @param {number} centerY y-center of the circle
 * @param {number} radius radius of the circle
 * @param {number} thickness the width of the border
 * @param {Array} lineDash the dashstyle of the border in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the circle
 * @param {number} startAngle the start angle of the circle in radians
 * @param {number} endAngle the end angle of the circle in radians
 */
export function drawCanvasCircleBorder (context, centerX, centerY, radius, thickness, lineDash, color, opacity = 1, startAngle = 0, endAngle = Math.PI * 2) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.setLineDash(lineDash)
  context.beginPath()
  context.arc(centerX, centerY, radius, startAngle, endAngle)
  context.stroke()
}
/**
 * Draws a filled rectangle with rounded corners onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} x x-coordinate of the rectangle's top-left corner
 * @param {number} y y-coordinate of the rectangle's top-left corner
 * @param {number} width width of the rectangle
 * @param {number} height height of the rectangle
 * @param {number} radius radius of the corners
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the rectangle
 * @param {Boolean} topLeft sets, if the top left corner should be curved
 * @param {Boolean} topRight sets, if the top right corner should be curved
 * @param {Boolean} bottomLeft sets, if the bottom left corner should be curved
 * @param {Boolean} bottomRight sets, if the bottom right corner should be curved
 */
export function drawCanvasRectRound (context, x, y, width, height, radius, color, opacity = 1, topLeft = true, topRight = true, bottomLeft = true, bottomRight = true, rotation = 0) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  if (rotation > 0) {
    context.translate(x, y)
    context.rotate(rotation * Math.PI / 180)
    context.translate(-x, -y)
  }
  context.beginPath()
  context.moveTo(x + radius, y)
  if (topRight) {
    context.lineTo(x + width - radius, y)
    context.arc(x + width - radius, y + radius, radius, Math.PI * 1.5, Math.PI * 2)
  } else {
    context.lineTo(x + width, y)
  }
  if (bottomRight) {
    context.lineTo(x + width, y + height - radius)
    context.arc(x + width - radius, y + height - radius, radius, 0, Math.PI * 0.5)
  } else {
    context.lineTo(x + width, y + height)
  }
  if (bottomLeft) {
    context.lineTo(x + radius, y + height)
    context.arc(x + radius, y + height - radius, radius, Math.PI * 0.5, Math.PI)
  } else {
    context.lineTo(x, y + height)
  }
  if (topLeft) {
    context.lineTo(x, y + radius)
    context.arc(x + radius, y + radius, radius, Math.PI, Math.PI * 1.5)
  } else {
    context.lineTo(x, y)
    context.closePath()
  }
  
  if (rotation > 0) {
    context.translate(x, y)
    context.rotate(-(rotation * Math.PI / 180))
    context.translate(-x, -y)
  }
  context.fill()
}

/**
 * Draws a border for a rectangle with rounded corners onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} x x-coordinate of the rectangle's top-left corner
 * @param {number} y y-coordinate of the rectangle's top-left corner
 * @param {number} width width of the rectangle
 * @param {number} height height of the rectangle
 * @param {number} radius radius of the corners
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} thickness the width of the line
 * @param {Array} lineDash the dashstyle of the rectangle in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {number} opacity the opacity of the rectangle
 * @param {Boolean} topLeft sets, if the top left corner should be curved
 * @param {Boolean} topRight sets, if the top right corner should be curved
 * @param {Boolean} bottomLeft sets, if the bottom left corner should be curved
 * @param {Boolean} bottomRight sets, if the bottom right corner should be curved
 */
export function drawCanvasRectRoundBorder (context, x, y, width, height, radius, color, thickness, lineJoin, lineDash, opacity = 1, topLeft = true, topRight = true, bottomLeft = true, bottomRight = true) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.lineJoin = lineJoin
  context.setLineDash(lineDash)
  context.beginPath()
  context.moveTo(x + radius, y)
  if (topRight) {
    context.lineTo(x + width - radius, y)
    context.arc(x + width - radius, y + radius, radius, Math.PI * 1.5, Math.PI * 2)
  } else {
    context.lineTo(x + width, y)
  }
  if (bottomRight) {
    context.lineTo(x + width, y + height - radius)
    context.arc(x + width - radius, y + height - radius, radius, 0, Math.PI * 0.5)
  } else {
    context.lineTo(x + width, y + height)
  }
  if (bottomLeft) {
    context.lineTo(x + radius, y + height)
    context.arc(x + radius, y + height - radius, radius, Math.PI * 0.5, Math.PI)
  } else {
    context.lineTo(x, y + height)
  }
  if (topLeft) {
    context.lineTo(x, y + radius)
    context.arc(x + radius, y + radius, radius, Math.PI, Math.PI * 1.5)
  } else {
    context.lineTo(x, y)
    context.closePath()
  }
  context.stroke()
}

/**
 * Measures the width of text on the canvas in px and returns the width
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {string} text measured text
 * @param {string} align alignment of the text ('left', 'right', 'center', 'start', 'end')
 * @param {string} baseline baseline of the text ('top', 'hanging', 'middle', 'alphabetic', 'ideographic', 'bottom')
 * @param {string} font font used for the text
 * @param {number} size the size in px for the text
 * @param {Boolean} bold if the text should be bold
 * @returns {TextMetrics} the measured width of the text
 */
export function measureCanvasText (context, text, align, baseline, font, size, bold) {
  context.textAlign = align
  context.textBaseline = baseline
  context.font = `${bold ? 'bold' : ''} ${size}px ${font}`.trim()
  return context.measureText(text).width
}

/**
 * Draw text onto the used canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {string} text text to be written
 * @param {number} x x-coordinate for the positioning point
 * @param {number} y y-coordinate for the positioning point
 * @param {string} align alignment of the text ('left', 'right', 'center', 'start', 'end')
 * @param {string} baseline baseline of the text ('top', 'hanging', 'middle', 'alphabetic', 'ideographic', 'bottom')
 * @param {string} font font used for the text
 * @param {number} size the size in px for the text
 * @param {Boolean} bold if the text should be bold
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the rectangle
 */
export function drawCanvasText (context, text, x, y, align, baseline, font, size, bold, color, opacity = 1) {
  context.textAlign = align
  context.textBaseline = baseline
  context.font = `${bold ? 'bold' : ''} ${size}px ${font}`.trim()
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.fillText(text, x, y)
}

/**
 * Draw a line with an adjustable amount of points onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} startX x-coordinate of the start point
 * @param {number} startY y-coordinate of the start point
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} thickness the width of the line
 * @param {string} cap the corner caps ('butt', 'round', 'square')
 * @param {string} lineJoin the outer corner style of the line ('round', 'bevel', 'miter')
 * @param {Array} lineDash the dashstyle of the border in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {number} opacity the opacity of the line
 * @param  {...number} points points of the line with x and y
 */
export function drawCanvasLine (context, startX, startY, color, thickness, cap, lineJoin, lineDash, opacity, ...points) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.lineCap = cap
  context.lineJoin = lineJoin
  context.setLineDash(lineDash)
  context.beginPath()
  context.moveTo(startX, startY)
  for (let i = 0; i < points.length / 2; i++) {
    if (points[i * 2] !== undefined && points[i * 2 + 1] !== undefined) {
      context.lineTo(points[i * 2], points[i * 2 + 1])
    } else {
      break
    }
  }
  context.stroke()
}

/**
 * Draw a line with an adjustable amount of bezier points onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} startX x-coordinate of the start point
 * @param {number} startY y-coordinate of the start point
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} thickness the width of the line
 * @param {string} cap the corner caps ('butt', 'round', 'square')
 * @param {string} lineJoin the outer corner style of the line ('round', 'bevel', 'miter')
 * @param {Array} lineDash the dashstyle of the border in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {number} opacity the opacity of the line
 * @param  {...number} points points of the line as an array with x and y of the control points and the next point
 */
export function drawCanvasBezierLine (context, startX, startY, color, thickness, cap, lineJoin, lineDash, opacity, ...points) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.lineCap = cap
  context.lineJoin = lineJoin
  context.setLineDash(lineDash)
  context.beginPath()
  context.moveTo(startX, startY)
  for (let i = 0; i < points.length; i++) {
    const point = points[i]
    if (point.length === 6) {
      context.bezierCurveTo(...point)
    } else if (point.length === 4) {
      context.bezierCurveTo(point[0], point[1], point[0], point[1], point[2], point[3])
    } else if (point.length === 2) {
      let prevPoint = [startX, startY]
      if (i > 0) {
        prevPoint = points[i - 1]
      }
      context.bezierCurveTo(
        prevPoint[prevPoint.length - 2], prevPoint[prevPoint.length - 1], point[0], point[1], point[0], point[1]
      )
    }
  }
  context.stroke()
}

/**
 * Draw a polygon onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} startX x-coordinate of the start point
 * @param {number} startY y-coordinate of the start point
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the line
 * @param  {...number} points points of the line with x and y
 */
export function drawCanvasPolygon (context, startX, startY, color, opacity, ...points) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.beginPath()
  context.moveTo(startX, startY)
  for (let i = 0; i < points.length / 2; i++) {
    if (points[i * 2] !== undefined && points[i * 2 + 1] !== undefined) {
      context.lineTo(points[i * 2], points[i * 2 + 1])
    } else {
      break
    }
  }
  context.fill()
}

/**
 * Draw a bezier polygon onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} startX x-coordinate of the start point
 * @param {number} startY y-coordinate of the start point
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the line
 * @param  {...Array} points points of the line as an array with x and y of the control points and the next point
 */
export function drawCanvasBezierPolygon (context, startX, startY, color, opacity, ...points) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.beginPath()
  context.moveTo(startX, startY)
  for (let i = 0; i < points.length; i++) {
    const point = points[i]
    if (point.length === 6) {
      context.bezierCurveTo(...point)
    } else if (point.length === 4) {
      context.bezierCurveTo(point[0], point[1], point[0], point[1], point[2], point[3])
    } else if (point.length === 2) {
      let prevPoint = [startX, startY]
      if (i > 0) {
        prevPoint = points[i - 1]
      }
      context.bezierCurveTo(
        prevPoint[prevPoint.length - 2], prevPoint[prevPoint.length - 1], point[0], point[1], point[0], point[1]
      )
    }
  }
  context.closePath()
  context.fill()
}

/**
 * Draw an ellipse with a custom shift onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} centerX x-coordinate for the positioning point
 * @param {number} centerY y-coordinate for the positioning point
 * @param {number} radiusV the radius of the vertical circle (defines top and bottom distances)
 * @param {number} radiusH the radius of the horizontal circle (defines the left and right distances)
 * @param {number} offsetVY the offset of the vertical circle in y direction
 * @param {number} offsetHX the offset of the horizontal circle in x direction
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the ellipse
 * @param {Array} cpDist the rounding intensity in the order of bottom-left, top-left, top-right, bottom-right
 */
export function drawCanvasEllipseCustom (context, centerX, centerY, radiusV, radiusH, offsetVY, offsetHX, color, opacity = 1, rotation = 0, cpDist = [0.5522848, 0.5522848, 0.5522848, 0.5522848]) {
  if (Array.isArray(color)) {
    context.fillStyle = `rgba(${color}, ${opacity})`
  } else {
    let linearGradient = context.createLinearGradient(
      centerX + color.start.x * (radiusH + offsetHX), centerY + color.start.y * (radiusV + offsetVY),
      centerX + color.end.x * (radiusH - offsetHX), centerY + color.end.y * (radiusV - offsetVY)
    )
    for (let stop of color.stops) {
      if (Array.isArray(stop.color)) {
        linearGradient.addColorStop(stop.offset, `rgba(${stop.color}, ${opacity})`)
      } else {
        linearGradient.addColorStop(stop.offset, stop.color)
      }
    }
    context.fillStyle = linearGradient
  }
  if (rotation > 0) {
    context.translate(centerX, centerY)
    context.rotate(rotation * Math.PI / 180)
    context.translate(-centerX, -centerY)
  }
  context.beginPath()
  context.moveTo(centerX, centerY + radiusV + offsetVY)
  context.bezierCurveTo(
    centerX - (radiusH - offsetHX) * cpDist[0], centerY + radiusV + offsetVY,
    centerX - radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[0],
    centerX - radiusH + offsetHX, centerY
  )
  context.bezierCurveTo(
    centerX - radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[1],
    centerX - (radiusH - offsetHX) * cpDist[1], centerY - radiusV + offsetVY,
    centerX, centerY - radiusV + offsetVY
  )
  context.bezierCurveTo(
    centerX + (radiusH + offsetHX) * cpDist[2], centerY - radiusV + offsetVY,
    centerX + radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[2],
    centerX + radiusH + offsetHX, centerY
  )
  context.bezierCurveTo(
    centerX + radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[3],
    centerX + (radiusH + offsetHX) * cpDist[3], centerY + radiusV + offsetVY,
    centerX, centerY + radiusV + offsetVY
  )
  
  if (rotation > 0) {
    context.translate(centerX, centerY)
    context.rotate(-(rotation * Math.PI / 180))
    context.translate(-centerX, -centerY)
  }
  context.fill()
}

/**
 * Draw an ellipse border with a custom shift onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} centerX x-coordinate for the positioning point
 * @param {number} centerY y-coordinate for the positioning point
 * @param {number} radiusV the radius of the vertical circle (defines top and bottom distances)
 * @param {number} radiusH the radius of the horizontal circle (defines the left and right distances)
 * @param {number} offsetVY the offset of the vertical circle in y direction
 * @param {number} offsetHX the offset of the horizontal circle in x direction
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} thickness the width of the line
 * @param {string} cap the corner caps ('butt', 'round', 'square')
 * @param {Array} lineDash the dashstyle of the ellipse in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {number} opacity the opacity of the ellipse
 * @param {number} rotation of the ellipse
 * @param {Array} cpDist the rounding intensity in the order of bottom-left, top-left, top-right, bottom-right
 */
export function drawCanvasEllipseCustomBorder (context, centerX, centerY, radiusV, radiusH, offsetVY, offsetHX, color, thickness, lineDash, opacity = 1, rotation = 0, cpDist = [0.5522848, 0.5522848, 0.5522848, 0.5522848]) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.setLineDash(lineDash)
  if (rotation > 0) {
    context.translate(centerX, centerY)
    context.rotate(rotation * Math.PI / 180)
    context.translate(-centerX, -centerY)
  }
  context.beginPath()
  context.moveTo(centerX, centerY + radiusV + offsetVY)
  context.bezierCurveTo(
    centerX - (radiusH - offsetHX) * cpDist[0], centerY + radiusV + offsetVY,
    centerX - radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[0],
    centerX - radiusH + offsetHX, centerY
  )
  context.bezierCurveTo(
    centerX - radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[1],
    centerX - (radiusH - offsetHX) * cpDist[1], centerY - radiusV + offsetVY,
    centerX, centerY - radiusV + offsetVY
  )
  context.bezierCurveTo(
    centerX + (radiusH + offsetHX) * cpDist[2], centerY - radiusV + offsetVY,
    centerX + radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[2],
    centerX + radiusH + offsetHX, centerY
  )
  context.bezierCurveTo(
    centerX + radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[3],
    centerX + (radiusH + offsetHX) * cpDist[3], centerY + radiusV + offsetVY,
    centerX, centerY + radiusV + offsetVY
  )
  
  if (rotation > 0) {
    context.translate(centerX, centerY)
    context.rotate(-(rotation * Math.PI / 180))
    context.translate(-centerX, -centerY)
  }
  context.stroke()
}

/**
 * Draws a heart onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} bottomX x-coordinate for the bottom point of the heart
 * @param {number} bottomY y-coordinate for the bottom point of the heart
 * @param {number} height height of the heart at the center (actual height will be higher)
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the heart
 */
export function drawCanvasHeart (context, bottomX, bottomY, height, color, opacity = 1) {
  const radius = Math.sqrt(Math.pow(height, 2) + Math.pow(height, 2))
  if (Array.isArray(color)) {
    context.fillStyle = `rgba(${color}, ${opacity})`
  } else {
    let linearGradient = context.createLinearGradient(
      bottomX + color.start.x, (bottomY - height / 2) + color.start.y, bottomX + color.end.x,
      (bottomY - height / 2) + color.end.y
    )
    for (let stop of color.stops) {
      if (Array.isArray(stop.color)) {
        linearGradient.addColorStop(stop.offset, `rgba(${stop.color}, ${opacity})`)
      } else {
        linearGradient.addColorStop(stop.offset, stop.color)
      }
    }
    context.fillStyle = linearGradient
  }
  context.beginPath()
  context.moveTo(bottomX, bottomY)
  context.lineTo(bottomX - Math.abs(height) / 2, bottomY - height / 2)
  if (height > 0) {
    context.arc(bottomX - Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 0.75, Math.PI * 1.75)
    context.arc(bottomX + Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 1.25, Math.PI * 2.25)
  } else {
    context.arc(bottomX - Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 1.25, Math.PI * 2.25, true)
    context.arc(bottomX + Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 0.75, Math.PI * 1.75, true)
  }
  context.fill()
}

/**
 * Draws a border in the shape of a heart onto the canvas
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} bottomX x-coordinate for the bottom point of the heart
 * @param {number} bottomY y-coordinate for the bottom point of the heart
 * @param {number} height height of the heart at the center (actual height will be higher)
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} thickness the width of the border
 * @param {string} cap the corner caps ('butt', 'round', 'square')
 * @param {Array} lineDash the dashstyle of the heart in the form of [draw, empty, draw, etc.]. An empty array means the border is solid
 * @param {number} opacity the opacity of the heart
 */
export function drawCanvasHeartBorder (context, bottomX, bottomY, height, color, thickness, cap, lineDash, opacity = 1) {
  const radius = Math.sqrt(Math.pow(height, 2) + Math.pow(height, 2))
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = thickness
  context.lineCap = cap
  context.setLineDash(lineDash)
  context.beginPath()
  context.moveTo(bottomX, bottomY)
  context.lineTo(bottomX - Math.abs(height) / 2, bottomY - height / 2)
  if (height > 0) {
    context.arc(bottomX - Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 0.75, Math.PI * 1.75)
    context.arc(bottomX + Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 1.25, Math.PI * 2.25)
  } else {
    context.arc(bottomX - Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 1.25, Math.PI * 2.25, true)
    context.arc(bottomX + Math.abs(height) / 4, bottomY - height * 0.75, radius * 0.25, Math.PI * 0.75, Math.PI * 1.75, true)
  }
  context.closePath()
  context.stroke()
}

/**
 * Returns a path2d object that has the path for a custom ellipse in it
 * @param {number} centerX x-coordinate for the positioning point
 * @param {number} centerY y-coordinate for the positioning point
 * @param {number} radiusV the radius of the vertical circle (defines top and bottom distances)
 * @param {number} radiusH the radius of the horizontal circle (defines the left and right distances)
 * @param {number} offsetVY the offset of the vertical circle in y direction
 * @param {number} offsetHX the offset of the horizontal circle in x direction
 * @param {Array} cpDist the rounding intensity in the order of bottom-left, top-left, top-right, bottom-right
 * @param {Boolean} anticlockwise if the circle should be drawn anticlockwise
 * @returns a new path2d object that has that path
 */
export function getCanvasEllipseCustomPath (centerX, centerY, radiusV, radiusH, offsetVY, offsetHX, cpDist = [0.5522848, 0.5522848, 0.5522848, 0.5522848], anticlockwise = false) {
  let path = new Path2D()
  path.moveTo(centerX, centerY + radiusV + offsetVY)
  if (!anticlockwise) {
    path.bezierCurveTo(
      centerX - (radiusH - offsetHX) * cpDist[0], centerY + radiusV + offsetVY,
      centerX - radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[0],
      centerX - radiusH + offsetHX, centerY
    )
    path.bezierCurveTo(
      centerX - radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[1],
      centerX - (radiusH - offsetHX) * cpDist[1], centerY - radiusV + offsetVY,
      centerX, centerY - radiusV + offsetVY
    )
    path.bezierCurveTo(
      centerX + (radiusH + offsetHX) * cpDist[2], centerY - radiusV + offsetVY,
      centerX + radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[2],
      centerX + radiusH + offsetHX, centerY
    )
    path.bezierCurveTo(
      centerX + radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[3],
      centerX + (radiusH + offsetHX) * cpDist[3], centerY + radiusV + offsetVY,
      centerX, centerY + radiusV + offsetVY
    )
  } else {
    path.bezierCurveTo(
      centerX + (radiusH + offsetHX) * cpDist[3], centerY + radiusV + offsetVY,
      centerX + radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[3],
      centerX + radiusH + offsetHX, centerY
    )
    path.bezierCurveTo(
      centerX + radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[2],
      centerX + (radiusH + offsetHX) * cpDist[2], centerY - radiusV + offsetVY,
      centerX, centerY - radiusV + offsetVY
    )
    path.bezierCurveTo(
      centerX - (radiusH - offsetHX) * cpDist[1], centerY - radiusV + offsetVY,
      centerX - radiusH + offsetHX, centerY - (radiusV - offsetVY) * cpDist[1],
      centerX - radiusH + offsetHX, centerY
    )
    path.bezierCurveTo(
      centerX - radiusH + offsetHX, centerY + (radiusV + offsetVY) * cpDist[0],
      centerX - (radiusH - offsetHX) * cpDist[0], centerY + radiusV + offsetVY,
      centerX, centerY + radiusV + offsetVY
    )
  }
  return path
}

/**
 * Draws a sparkle or rather a rectangle with inverted rounded corners
 * @param {CanvasRenderingContext2D} context the context of the canvas (canvas.getContext('2d'))
 * @param {number} x x-coordinate of the rectangle's top-left corner
 * @param {number} y y-coordinate of the rectangle's top-left corner
 * @param {number} width width of the rectangle
 * @param {number} height height of the rectangle
 * @param {number} radiusV the radius of the vertical center of the curve (defines top and bottom distances)
 * @param {number} radiusH the radius of the horizontal center of the curve (defines the left and right distances)
 * @param {Array} color array of the three rgb values required for the color
 * @param {number} opacity the opacity of the sparkle
 * @param {number} rotation the rotation of the sparkle
 * @param {Array} cpDist the rounding intensity in the order of bottom-left, top-left, top-right, bottom-right
 */
export function drawCanvasSparkle (context, x, y, width, height, radiusH, radiusV, color, opacity = 1, rotation = 0, cpDist = [0.5522848, 0.5522848, 0.5522848, 0.5522848]) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  if (rotation > 0) {
      context.translate(x + width / 2, y + height / 2)
      context.rotate(rotation * Math.PI / 180)
      context.translate(-(x + width / 2), -(y + height / 2))
  }
  context.beginPath()
  context.moveTo(x + radiusH, y)
  context.lineTo(x + width - radiusH, y)
  context.bezierCurveTo(
      x + width - radiusH, y + radiusV * cpDist[0],
      x + width - radiusH * cpDist[0], y + radiusV,
      x + width, y + radiusV
  )
  context.lineTo(x + width, y + height - radiusV)
  context.bezierCurveTo(
      x + width - radiusH * cpDist[1], y + height - radiusV,
      x + width - radiusH, y + height - radiusV * cpDist[1],
      x + width - radiusH, y + height
  )
  context.lineTo(x + radiusH, y + height)
  context.bezierCurveTo(
      x + radiusH, y + height - radiusV * cpDist[2],
      x + radiusH * cpDist[2], y + height - radiusV,
      x, y + height - radiusV
  )
  context.lineTo(x, y + radiusV)
  context.bezierCurveTo(
      x + radiusH * cpDist[3], y + radiusV,
      x + radiusH, y + radiusV * cpDist[3],
      x + radiusH, y
  )
  if (rotation > 0) {
      context.translate(x + width / 2, y + height / 2)
      context.rotate(-(rotation * Math.PI / 180))
      context.translate(-(x + width / 2), -(y + height / 2))
  }
  context.fill()
}